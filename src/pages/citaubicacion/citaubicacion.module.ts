import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitaubicacionPage } from './citaubicacion';

@NgModule({
  declarations: [
    CitaubicacionPage,
  ],
  imports: [
    IonicPageModule.forChild(CitaubicacionPage),
  ],
})
export class CitaubicacionPageModule {}
