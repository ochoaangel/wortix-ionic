import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DoctorsProvider } from '../../providers/doctors/doctors';
import { IssuesProvider } from '../../providers/issues/issues';
import { LocationProvider } from '../../providers/location/location';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilProvider } from '../../providers/util/util';


/**
 * Generated class for the IssueInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-issue-info',
  templateUrl: 'issue-info.html',
})
export class IssueInfoPage {
	issue:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public util: UtilProvider, public providerDoctor: DoctorsProvider, public serviceLocation: LocationProvider, public auth:AuthProvider, public issuesProvider: IssuesProvider) {  
	this.issue = this.navParams.get('issue') || JSON.parse(localStorage.issue) || {};
	localStorage.issue = JSON.stringify(this.issue);
	console.log(this.issue);
  }

  ionViewDidLoad() {
	  if (this.issue.type == 'house-visit'){
		  this.iniciarMapa()
	  }
	
	this.providerDoctor.getMyChatMessages();
	
	setTimeout(()=>  {
				  this.issuesProvider.get_issue_cometarios(this.issue);
				}, 300)
  }
  
  iniciarMapa(){
	this.serviceLocation.noSearch = true;
	this.serviceLocation.clearMap();
    console.log('ionViewDidLoad IssueInfoPage');
	this.serviceLocation.iniciarMapa().then( data => {
		this.serviceLocation.pinblueSTC(this.issue.latitude, this.issue.longitude)
		if(this.serviceLocation.map!=null){
			this.serviceLocation.map.setDiv('map_canvas');
			this.serviceLocation.map.setVisible(true);
			setTimeout(()=>{ 
			   try{
				  //this.serviceLocation.putCircle()
				   this.serviceLocation.map.setDiv('map_canvas');
				   this.serviceLocation.map.setVisible(true);
				  
			   }catch(e){
				   alert(e);
			   }
			},1000);
		}
	})
	  
  }
  
  	goToDoctor(doctor, type){
		if (type == 'doctor'){
			this.providerDoctor.get_doctor_one(doctor.id).then( doctor=>{
					console.log( doctor.data[0]);
					this.navCtrl.push('ProfilePage',{doctor: doctor.data[0], type: type});
				}
			)
		}
		
	}
	
	comentario:string="";
	addComentario(comentario){
		if (comentario == ""){
			return;
		} 
		this.comentario = "";
		this.issuesProvider.comentar(this.issue.id, comentario, this.file).then(data => {
			this.issuesProvider.get_issue_cometarios(this.issue);
			localStorage.issue = JSON.stringify(this.issue);
		})
		
		
	}
	
	getChat(){
		let chat = null;
		for (let i = 0; i <  this.providerDoctor.chatsRooms.length; i++){
			if (this.providerDoctor.chatsRooms[i].cita && this.providerDoctor.chatsRooms[i].cita.id == this.issue.id){
				 chat = this.providerDoctor.chatsRooms[i];
			}
		}
		if (!chat){
			this.util.showToast("El chat para este caso de salud no esta disponible" ,"bottom",'Ok',2000);
		}else{
			this.navCtrl.push("ChatUserPage", {room: chat});
		}
		
	}
	
	
	getFiles(){
			this.navCtrl.push("FileUploadPage", {issue: this.issue});
	}
	
	cambiarEstado(estado){
		alert(estado);
		this.issuesProvider.move(this.issue.id, estado).then(data => {
			//this.issuesProvider.get_issue_cometarios(this.issue);
			localStorage.issue = JSON.stringify(this.issue);
		})
		
		
	}
	
	file: File;
	changeListener($event) : void {
		this.file = $event.target.files[0];
	}
	
	takeFile(){
		//@ts-ignore
		//console.log($("input"));
	}
	
	
   
}
