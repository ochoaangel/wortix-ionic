import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { InfouserPage } from './infouser';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    InfouserPage,
  ],
  imports: [
    IonicPageModule.forChild(InfouserPage), TranslateModule
  ],
})
export class InfouserPageModule {}
