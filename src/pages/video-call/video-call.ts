import { Component } from '@angular/core';
import {IonicPage,  NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Platform } from 'ionic-angular';

declare var iosrtc;
declare var apiRTC;
declare var apiCC;

const STATE_WAIT = "wait";
const STATE_INCALL = "incall";

const LABEL_CALL = "Call";
const LABEL_HANGOUT = "Hangout";

const COLOR_CALL = "#5cb85c";
const COLOR_HANGOUT = "#d9534f";

@IonicPage()
@Component({
  selector: 'page-video-call',
  templateUrl: 'video-call.html',
})
export class VideoCallPage {

  distantNumber:any;
  webRTCClient:any;
  infoLabel:any;
  buttonLabel:any;
  buttonColor:any;
  state:any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public platform: Platform) {

    this.incomingCallHandler = this.incomingCallHandler.bind(this);
    this.userMediaErrorHandler = this.userMediaErrorHandler.bind(this);
    this.remoteStreamAddedHandler = this.remoteStreamAddedHandler.bind(this);
    this.hangupHandler = this.hangupHandler.bind(this);
    this.refreshVideoView = this.refreshVideoView.bind(this);
    this.sessionReadyHandler = this.sessionReadyHandler.bind(this);
    this.userMediaSuccessHandler = this.userMediaSuccessHandler.bind(this);
    if (this.platform.is('cordova')){
      apiRTC.init({
        onReady: this.sessionReadyHandler,
        apiKey: "90c8c23592c4e936277d44f1138c7347"
      });
    }else{
      setTimeout( ()=>{
      apiRTC.addEventListener("incomingCall", this.incomingCallHandler);
      apiRTC.addEventListener("userMediaError", this.userMediaErrorHandler);
      apiRTC.addEventListener("remoteStreamAdded", this.remoteStreamAddedHandler);
      apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler);
      apiRTC.addEventListener("hangup", this.hangupHandler); 
      }, 3000)  

    }

    this.infoLabel= "Registration Ongoing...";
    this.buttonLabel = LABEL_CALL;
    this.buttonColor = COLOR_CALL;
    this.state = STATE_WAIT;
  }

  ionViewWillEnter(){
    setTimeout( ()=>{
      apiRTC.addEventListener("incomingCall", this.incomingCallHandler);
      apiRTC.addEventListener("userMediaError", this.userMediaErrorHandler);
      apiRTC.addEventListener("remoteStreamAdded", this.remoteStreamAddedHandler);
      apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler);
      apiRTC.addEventListener("hangup", this.hangupHandler); 
      this.webRTCClient = apiCC.session.createWebRTCClient({});
       this.infoLabel = "Your local ID : "+apiCC.session.apiCCId;
      }, 3000) 
  }

  /**
   * Call Action
   */
  pushCall(event) {
    console.log("Push, callState="+this.state);
    if(this.distantNumber && this.state == STATE_WAIT) {
      setTimeout(this.refreshVideoView,4000);
      this.webRTCClient.call(this.distantNumber);
    } else if(this.state == STATE_INCALL) {
      this.state = STATE_WAIT;
      this.buttonColor = COLOR_CALL;
      this.buttonLabel = LABEL_CALL;
      this.webRTCClient.hangUp();
    }
  }

  sessionReadyHandler(e) {
 ///   console.log("sessionReadyHandler");
    apiRTC.addEventListener("incomingCall", this.incomingCallHandler);
    apiRTC.addEventListener("userMediaError", this.userMediaErrorHandler);
    apiRTC.addEventListener("remoteStreamAdded", this.remoteStreamAddedHandler);
    apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler);
    apiRTC.addEventListener("hangup", this.hangupHandler);
    this.webRTCClient = apiCC.session.createWebRTCClient({});
    this.infoLabel = "Your local ID : "+apiCC.session.apiCCId;
  }

  refreshVideoView() {
    if (this.platform.is('ios')) {
      console.log("REFRESH");
      iosrtc.refreshVideos();
    }
  }

  incomingCallHandler(e) {
    console.log("incomingCallHandler");
    this.state = STATE_INCALL;
    this.buttonColor = COLOR_HANGOUT;
    this.buttonLabel = LABEL_HANGOUT;
    setTimeout(this.refreshVideoView,2000);
  }

  hangupHandler(e) {
    console.log("hangupHandler");
    this.state = STATE_WAIT;
    this.buttonColor = COLOR_CALL;
    this.buttonLabel = LABEL_CALL;
    this.initMediaElementState(e.detail.callId);
  }

  userMediaSuccessHandler(e) {
    console.log("userMediaSuccessHandler",e);
    this.webRTCClient.addStreamInDiv(
      e.detail.stream,
      e.detail.callType,
      "mini",
      'miniElt-' + e.detail.callId,
      {width : "128px", height : "96px"},
      true
    );
    
  }

  userMediaErrorHandler(e) {
    console.log(e);
    this.initMediaElementState(e.detail.callId);
  }

  remoteStreamAddedHandler(e) {
    console.log("remoteStreamAddedHandler",e);
    this.state = STATE_INCALL;
    this.buttonColor = COLOR_HANGOUT;
    this.buttonLabel = LABEL_HANGOUT;
    this.webRTCClient.addStreamInDiv(
      e.detail.stream,
      e.detail.callType,
      "remote",
      'remoteElt-' + e.detail.callId,
      {},
      false
    );
    setTimeout(this.refreshVideoView,1000);
  }

  initMediaElementState(callId) {
    this.webRTCClient.removeElementFromDiv('mini', 'miniElt-' + callId);
    this.webRTCClient.removeElementFromDiv('remote', 'remoteElt-' + callId);
  }
}
