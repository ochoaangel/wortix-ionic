import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';


import { GoogleMaps } from "@ionic-native/google-maps";
import { PayPal } from '@ionic-native/paypal';
import { Stripe } from '@ionic-native/stripe';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';


//Firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import { FIREBASE_CONFIG } from './app.firebase';
import { AppContants } from './app.constants';

import { Firebase } from '@ionic-native/firebase';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { MediaCapture } from  '@ionic-native/media-capture';
import { File } from '@ionic-native/file';
import { Crop } from '@ionic-native/crop';

import { IonicImageLoader } from 'ionic-image-loader';

import { LocationProvider } from '../providers/location/location';
import { UserproviderProvider } from '../providers/userprovider/userprovider';
import { NotificationproviderProvider } from '../providers/notificationprovider/notificationprovider';
import { AuthProvider } from '../providers/auth/auth';
import { SpecialiteProvider } from '../providers/specialite/specialite';
import { DoctorsProvider } from '../providers/doctors/doctors';
import { IssuesProvider } from '../providers/issues/issues';
import { PlacesProvider } from '../providers/places/places';
import { UtilProvider } from '../providers/util/util';
import { PagoProvider } from '../providers/pago/pago';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import {  HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function setTranslateLoader(http: HttpClient) {
 return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import {
 /* MatAutocompleteModule,
  //MatBadgeModule,
  //MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,*/
  MatProgressBarModule,
  MatProgressSpinnerModule,
 /* MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  //MatTreeModule,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher*/
} from '@angular/material';
import { FilesProvider } from '../providers/files/files';


const materialModules = [
  MatProgressBarModule,
  MatProgressSpinnerModule
];

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
	IonicImageLoader.forRoot(),
	 TranslateModule.forRoot({
  loader: {
   provide: TranslateLoader,
   useFactory: (setTranslateLoader),
   deps: [HttpClient]
 }
}),
	 AngularFireModule.initializeApp(FIREBASE_CONFIG[AppContants.config_mode]),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
	materialModules,
	HttpClientModule,
    AngularFirestoreModule.enablePersistence()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
	
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    LocationProvider,
    UserproviderProvider,
	GoogleMaps,
	PayPal,
  Camera,
  MediaCapture,
	File,
	Crop,
	Facebook,
	GooglePlus,
	AngularFireStorage,
	Firebase,
    NotificationproviderProvider,
    AuthProvider,
    SpecialiteProvider,
    DoctorsProvider,
    IssuesProvider,
    PlacesProvider,
    UtilProvider,
    PagoProvider,
    FilesProvider,
	
	
  ]
})
export class AppModule {
	
	
}
