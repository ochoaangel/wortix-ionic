import { Component, NgZone, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Content } from 'ionic-angular';
import { DoctorsProvider } from '../../providers/doctors/doctors'
import { PlacesProvider } from '../../providers/places/places';

/**
 * Generated class for the ListsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lists',
  templateUrl: 'lists.html',
})
export class ListsPage {
	
	@ViewChild(Content) content: Content;
	
	stars = [1,2,3,4,5];
	seach:any=[];       // LISTADO DE BUSQUEDA
	places:any=[];      // LISTADO DE LUGARES
	users:any=[];       // LISTADO DE DOCTORES
	tipo:any;           // FILTRO  DE TIPO {1: 'DOCTOR', 2: 'CLINICAS', 3: 'SEACRH'}
	barra:any=true;     // MUESTRA LA  BARRA DE PROXIMIDAD
	follows:any=false;  // MUESTRA EL FILTRO DE FOLLOW
	keyword:string="";  // KEYWORD PALABRA A BUSCAR
	
  constructor(public navCtrl: NavController,
			  public navParams: NavParams,
			  public actionSheetCtrl: ActionSheetController,
			  public providerDoctors: DoctorsProvider, 
			  public providerPlaces: PlacesProvider,
			   private zone: NgZone) {
		this.tipo = this.navParams.get('tipo') || localStorage.tipo || false;
		
		if (!this.tipo){
			this.tipo =1;
		}
		 localStorage.tipo = this.tipo;
		
		if (this.navParams.get('keyword')){
			this.keyword = this.navParams.get('keyword');
		}
		this.buscar();
		if (this.navParams.get('barra') == false){
			this.barra = this.navParams.get('barra');
			console.log(this.barra);
		}
		if (this.navParams.get('follows') == true){
			this.follows = this.navParams.get('follows');
		}
		
		
		
  }
  
  loadDoctors=false;
  meta:any;
  links:any; 
  metaP:any;
  linksP:any;
  buscar(event?:any, more?:boolean){
		this.barra= false;
		
		if (this.keyword== ""  && this.tipo == 3){
			return;
		}
		if (!more){
			this.users = [];
			this.seach = [] ;
			this.places = [] ;
		}
		this.zone.run(() => { this.loadDoctors = true;});
		if (this.tipo == 1 || this.tipo == 3){
			let promise:Promise<any>=null;
			if (!more){
				promise =this.providerDoctors.get_doctor_search(this.keyword, "consultation-room");
			}else{
				 this.zone.run(() => {
					promise =this.providerDoctors.get_doctor_search(this.keyword, "consultation-room", this.links.next);
				 })
			}
			promise.then( data=>{
				 this.zone.run(() => {
					 
					data.data = this.providerDoctors.transformDoctors(data.data);
					console.log(data.data);
					this.users = this.users.concat(data.data);
					this.seach = this.seach.concat(data.data);
					this.loadDoctors = false;
					this.meta = data.meta;
					this.links = data.links;
				});
					
			});
		}
		if (this.tipo == 2 ||this.tipo == 3){
			let promiseP:Promise<any>=null;
			if (!more){
				promiseP =this.providerPlaces.get_places_search(this.keyword);
			}else{
				 this.zone.run(() => {
					promiseP =this.providerPlaces.get_places_search(this.keyword, this.linksP.next);
				 })
			}
			promiseP.then( data=>{

				 this.zone.run(() => {
					 data.data = this.providerPlaces.transformPlaces(data.data);
					this.places = this.places.concat(data.data);
					this.seach = this.seach.concat(data.data);
					this.loadDoctors = false;
					this.metaP = {
						last_page: data.last_page,
						from: data.from,
						per_page:data.per_page,
						to:data.to,
						total: data.total,
						current_page: data.current_page
					};
					this.linksP ={
							last:data.last_page_url,
							prev: data.prev_page_url,
						    next: data.next_page_url,
							first: data.first_page_url,
							
					};
				})
			});
		}
  }

  ionViewDidLoad() {
    console.log(this.users);
  }
  
    presentActionSheet(doctor:any, type: string) {
		const actionSheet = this.actionSheetCtrl.create({
		  
		  buttons: [
			{
			  text: 'Favorito',
			  role: 'Privacy',
			  cssClass: 'btn-action',
			  handler: () => {
				this.providerDoctors.setFavoritos(doctor)
			  }
			}, {
			  text: 'Contact',
			  cssClass: 'btn-action',
			  handler: () => {
				this.navCtrl.push("NuevaCitaPage", {doctor: doctor.data, tipo:1});
			  }
			}, {
			  text: 'Profile',
			  cssClass: 'btn-action',
			  handler: () => {
				this.goToDoctor(doctor, type);
			  }
			  },
			  
			 
			
		  ]
		});
		actionSheet.present();
	}
	
	goToDoctor(nodo, type){
		this.navCtrl.push('ProfilePage',{doctor:nodo, type: type});
		
	}
	end($event){
		//console.log($event, this.content.getContentDimensions());
		let screen = this.content.getContentDimensions();
		if (!this.loadDoctors && $event.directionY=="down" && ((screen.scrollHeight - (screen.contentHeight*1.5)) < screen.scrollTop) ){
			this.buscar(null,true);
		}
	}
	
	 goToProfile(doctor:any, type: string){
		this.navCtrl.push('ProfilePage',{doctor:doctor, type: type});
	 }

}
