import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ModalController, NavParams } from 'ionic-angular';
import { auth } from 'firebase/app';
import { UserproviderProvider } from '../userprovider/userprovider';
import { AppContants } from '../../app/app.constants';
import { User } from '../../interfaces/user';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import * as firebase from 'firebase';
import { GooglePlus } from '@ionic-native/google-plus';
import { UtilProvider} from '../util/util';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Firebase } from '@ionic-native/firebase';

import { Subject } from 'rxjs';
import { resolve } from 'url';
import { rejects } from 'assert';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {




	log: Subject<number> = new Subject<number>();;
	isLogin:boolean=false;
	userL:any;
	authAct:any;
	promiseAuth:any;
	constructor(private fb: Facebook, public firebase: Firebase, public afs: AngularFirestore,   private google:GooglePlus, public http: HttpClient, public afAuth: AngularFireAuth,public modalCtrl: ModalController, public userProv: UserproviderProvider, public util: UtilProvider) {
		
		this.userL = this.getUser();
	}
  
  
    /*ESCUCHA LAS NOTIFICACIONES ACTUALMENTE ACTIVAS*/
	public async isAuthenticated(authStatus) {
		this.afAuth.auth.onAuthStateChanged(auth => {
		  try{
			let session = {uid:"",email:""};
			if(auth){
			  session = {uid:auth.uid,email:auth.email};
			}
			authStatus(session);
		  }catch(e){
			console.log(e);
		  }
		});
	}
	
	/* GUARDA EL USUARIO EN EL LOCALSTORAGE*/
	public userSave(user:any){
		//userL.uid = authAct.uid;
		localStorage.userL =  JSON.stringify(user);
		this.userL = user;
	}
	
	
	/* OBTIEN EL USUARIO GUARDADO*/
	public getUser(){
		try{
			return JSON.parse(localStorage.userL );
		}catch(e){
			return null;
		}
	}
	
	
	public authReady():Promise<any>{
		if (this.promiseAuth == undefined){
				return new Promise ( (resolve, rejected) =>{
					this.log.subscribe({
							next: (v) => {
								this.promiseAuth.then(data => resolve(data));
								this.log.complete();
							}
					})
				});
		}else{
			return this.promiseAuth;
		}
		
	}
	public userObserver(){
		  console.log();
		this.promiseAuth = new Promise ((resolve, rejected) => {
				this.isAuthenticated(auth => {  
					console.log(auth,  firebase.auth.EmailAuthProvider);
					this.authAct= auth;
					if (auth.uid && auth.email){
						this.isLogin = true;
					}else{
						this.isLogin = false;
					}
					resolve(this.isLogin);
				})
		})
		this.promiseAuth.then(()=> {});
		this.log.next(1);
		
	}
	
	/* DESLOGUEA EL USUARIO DE FIREBASE*/
	public signOut(): Promise<boolean> {
		return new Promise((resolve, rejected) => {
			
		  this.afAuth.auth.signOut().then(a => {
			  localStorage.clear();
				this.isLogin = false;
		  }).catch(e=>{console.log(e)
				
				  this.util.showToast("Ha ocurrido un error mientras se intetaba cerrar su sesion" ,"bottom",'Ok',2000)
		  
		  });
		});
	}
 
 
	/* REVISA SI EL USUARIO EXISTE SINO MANDA */
	public reviewUser(){
	  return new Promise( (resolve, rejeted) => {

			if (!this.isLogin ){
				let profileModal = this.modalCtrl.create('RegisterPage', { userId: 8675309 });
				
				profileModal.onDidDismiss(data => {
					resolve(this.isLogin);
				});
				profileModal.present();
			}else{
				resolve(true);
			}
	  })
	  
	
	}
  
  public googleRegister(f:boolean){
	  return new Promise((resolve, rejected) => {
		  let prov:any = null;
		  if (f == true){
			  
			 prov = new auth.FacebookAuthProvider();
			 this.fb.logout();
			 this.fb.login([])
			  .then((res: FacebookLoginResponse) => {
				 const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
				this.afAuth.auth.signInWithCredential(credential)
				  .then((response) => {
					  console.log(response);
					
					
				  })
			  
			  })
			.catch(e =>{ 
				this.util.showToast("Ha ocurrido un error mientras se intetaba  su sesion con Facebook" ,"bottom",'Ok',2000)
			});
		  }else{
			   prov = new auth.GoogleAuthProvider();
				let params;
				params = {
					'webClientId': '1066251335783-7k2ijkc3h5mis1on4d366vf5b1cj3pl9.apps.googleusercontent.com',
					'offline': true
				}
				this.google.login(params).then((response) => {;
					const { idToken, accessToken } = response
					this.onLoginSuccess(idToken, accessToken).then(auth => {
							resolve(auth);
						}
					);
				  }).catch((error) => {
					console.log(error)
					this.util.showToast("Ha ocurrido un error mientras se intetaba  su sesion con Google" ,"bottom",'Ok',2000)
				  })
			  
		  }
	  })	  
		  
  }
  
   public onLoginSuccess(accessToken, accessSecret) {
		return new Promise((resolve, rejected) => {	
			const credential = accessSecret ? firebase.auth.GoogleAuthProvider.credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider.credential(accessToken);
			console.log(credential);
			this.afAuth.auth.signInWithCredential(credential).then((response) => {
				 //console.log("respuesta", response, credential);
				 //alert(credential);
				this.sendTokenUser(accessToken).then( () => {
					//alert("auth");
					
					resolve(response);
					
				 });
				 
				 let a:any = response.getIdTokenResult();
				 let b:any = response.getIdToken();
			setTimeout(()=>{
				console.log(a, b)
				//@ts-ignore
				this.sendTokenUser(a.i.token).then( () => {
					//alert("auth");
					resolve(auth);
					
				});
			
			}, 5000);
				
			  })
		});

	}
	  
	  
	  public loginWithCredential(credential){
		/*  return  new Promise((resolve, rejected) => {	
			const credential = accessSecret ? firebase.auth.GoogleAuthProvider.credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider.credential(accessToken);
			this.afAuth.auth.signInWithCredential(credential).then((response) => {
				 //console.log("respuesta", response, credential);
				 //alert(credential);
				this.sendTokenUser(accessToken).then( () => {
			
					resolve(response);
					
				 });
				 
				 let a:any = response.getIdTokenResult();
				 let b:any = response.getIdToken();
				setTimeout(()=>{
					console.log(a, b)
					//@ts-ignore
					this.sendTokenUser(a.i.token).then( () => {
						//alert("auth");
						resolve(auth);
						
					});
				
				}, 5000);
				
			  })
			});*/
		  
	  }
 
  public singInByEmail(user_name: string, password: string): Promise<any> {
	 // alert(user_name); alert( password);
	 this.util.load(1);
    return new Promise((resolve, rejected) => {
		this.afAuth.auth.signInWithEmailAndPassword(user_name, password).then(auth => {
			console.log("auths", auth);
			
			console.log("credential", firebase.auth.EmailAuthProvider.credential(user_name, password));
			//@ts-ignore
			let a:any = auth.user.getIdTokenResult();
			
				//@ts-ignore
				this.loginUser(user_name, password).then(  data => {
					this.userSave(data);
					this.util.unload(1);
					resolve(auth);
					
				});
			
			
			
      }).catch(error => {
		  this.util.unload(1);
        console.log(error+" "+error.code)
        if (error.code == 'auth/invalid-email') {
          rejected('Usuario y/o contraseña incorrectos. Por favor, ingrese sus datos correctamente.');
        } else if (error.code == 'auth/user-disabled') {
          rejected('Usuario desactivado.');
        } else if (error.code == 'auth/user-not-found') {
          rejected('Usuario no existe. Por favor, regístrese.');
        } else if (error.code == 'auth/wrong-password') {
          rejected('Usuario y/o contraseña incorrectos. Por favor, ingrese sus datos correctamente.');
        } else if (error == 'Error: A network error (such as timeout, interrupted connection or unreachable host) has occurred.') {
          rejected("No se detecta conexión a internet. Por favor, verifique su conexión e intente de nuevo.");
        }
      });
    });
	
  }
  
	/*
		1:patient
		2:doctor
	*/
  
	public signUpByEmail(user_data:any, tipo:number){
		this.util.load(1);
	    return new Promise((resolve, rejected) => {
			
		  this.afAuth.auth.createUserWithEmailAndPassword(user_data.email, user_data.password).then(auth => {
				if(tipo == 1){ 
					this.userProv.createUser(user_data,0).then( (data:any) => {
							this.userSave(data);
							this.util.unload(1);
						resolve(auth);
						
					})
				}else{
					//user_data.firebaseToken = firebase.auth.EmailAuthProvider.credential(user_data.email, user_data.password);
					this.userProv.createUserDoctor(user_data,0).then( (data:any) => {
							this.userSave(data);
							this.util.unload(1);
						resolve(auth);
					});
					
					
				}
			
			
		  }).catch(error => {
			console.log(error+" "+error.code)
			if (error.code == 'auth/invalid-email') {
			  rejected('Usuario y/o contraseña incorrectos. Por favor, ingrese sus datos correctamente.');
			} else if (error.code == 'auth/user-disabled') {
			  rejected('Usuario desactivado.');
			} else if (error.code == 'auth/user-not-found') {
			  rejected('Usuario no existe. Por favor, regístrese.');
			} else if (error.code == 'auth/wrong-password') {
			  rejected('Usuario y/o contraseña incorrectos. Por favor, ingrese sus datos correctamente.');
			} else if (error == 'Error: A network error (such as timeout, interrupted connection or unreachable host) has occurred.') {
			  rejected("No se detecta conexión a internet. Por favor, verifique su conexión e intente de nuevo.");
			} else if (error.code == 'auth/email-already-in-use'){
			  rejected("El Correo usado ya esta registrado en WORTIX.");
			}else{
				rejected("Ha ocurrido un error mientras se intentaba registrar, por favor, verifique sus datos e intentelo nuevamenta")
				
			}
				this.util.unload(1);
			
		  });
		});  
	}

	loginUser(user_name: string, password: string){
		let send = {
			email : user_name,
			password : password
			
		}
		return new Promise( (resolve, rejected) => {
			this.http.post( AppContants.url_backend + "api/login", send).toPromise().then(
			  (data:any) =>{
				console.log(data);
				resolve(data);
			}).catch(err => {
				console.error(err);
				rejected(err);
			});
			
		})
	}
	
	
	sendTokenUser(token: string){
		let send = {
			firebaseToken : token,
			
		}
		return new Promise( (resolve, rejected) => {
			this.http.post( AppContants.url_backend + "api/login", send).toPromise().then(
			  (data:any) =>{
				  this.whoIam();
				if (data.user.role == 'doctor'){
					//this.afs.collection('doctors').doc(this.authAct.uid).collection('doctors').doc(cita.doctor.id+"").collection('citas').doc(uid).set(cita);
						
				}
				resolve(data);
			}).catch(err => {
				console.error(err);
				rejected(err);
			});
			
		})
	}
	
	issuesPromise:any;
	saveMeLatLong(lat:number, lng:number, reach:number){
		let data={
			 api_token: this.userL.user.api_token,
			 gender: this.userL.user.gender,
			 last_name: this.userL.user.last_name,
			 name: this.userL.user.name,
			mobile:this.userL.user.mobile,
			phone:this.userL.user.phone,
			latitude:lat,
			longitude: lng,
			reach_attention: reach
		}
	//alert("pass");
		let url =  AppContants.url_backend + "/api/profile";
		this.util.load(1);
		//data.api_token= 
		let promise = this.http.put(url, data).toPromise().then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			//this.issues =data;
		}).catch(err => {
			this.util.showToast("Ha ocurrido un error mientras se intetaba guardar su ubicacion" ,"bottom",'Ok',2000)
			console.error(err);
			 this.util.unload(1);
		});
		return promise;
	}
	
	
	saveMe(tipos?){
		let data:any={
			api_token: this.userL.user.api_token,
			gender: this.userL.user.gender,
			last_name: this.userL.user.last_name,
			name: this.userL.user.name,
			mobile:this.userL.user.mobile,
			phone:this.userL.user.phone,
			birthdate:this.userL.user.bith_date,
			address:this.userL.user.address,
			dni:this.userL.user.dni,
			display_name:this.userL.user.display_name,
			specialty_id:this.userL.user.specialty_id,
		}
		if (tipos){
			data.types = tipos;
		}
		let url =  AppContants.url_backend + "/api/profile";
		this.util.load(1);
		//data.api_token= 
		let prom = this.http.put(url, data).toPromise();
		prom.then(
		  (data:any) =>{
			  Object.assign(this.userL.user, data.profile);
			  	this.whoIam();
			  this.util.unload(1);
			  
		
			//this.issues =data;
		}).catch(err => {
			this.util.showToast("Ha ocurrido un error mientras se intetaba guardar sus datos" ,"bottom",'Ok',2000)
			console.error(err);
			 this.util.unload(1);
		});
		 return prom;
		
	}
	
	/*changePhoto(url: string){
		let data:any={
			api_token: this.userL.user.api_token,
			 gender: this.userL.user.gender,
			 last_name: this.userL.user.last_name,
			 name: this.userL.user.name,
			mobile:this.userL.user.mobile,
			phone:this.userL.user.phone,
			latitude:lat,
			longitude: lng,
			reach_attention: reach
		}
		
		let formData = new FormData();
		if (file)
			formData.append('file', file, file.name);
		formData.append('api_token', this.auth.userL.user.api_token);
	}*/
	
	whoIam(){
		let data={
			 api_token: this.userL.user.api_token
		}
		let url =  AppContants.url_backend + "/api/profile?api_token="+this.userL.user.api_token;
		this.util.load(1);
		//data.api_token= 
		let prom = this.http.get(url).toPromise();
		prom.then(
		  (data:any) =>{
				Object.assign(this.userL.user, data.profile);
				let dA = data.profile.birthdate.split("/");
					this.userL.user.birthdate = (new Date(dA[1]+"/"+dA[0]+"/"+dA[2])).toISOString();
				
				this.userSave(this.userL);
				console.log(data)
			  this.util.unload(1);
				this.firebase.getToken().then(token => {
					alert(token);
					if (token)
						this.userL.token = token;
					this.afs.collection('users').doc(this.authAct.uid).update(this.userL).then(()=> {}).catch(e => { this.afs.collection('users').doc(this.authAct.uid).set(this.userL)});
				});
			//this.issues =data;
		}).catch(err => {
			this.util.showToast("Ha ocurrido un error mientras se intetaba recuperar su informacion" ,"bottom",'Ok',2000);
			console.error(err);
			 this.util.unload(1);
		});
		return prom;
	}


	newVideo(url){
		let uid = this.afs.createId();
		let video = {
			user_uid: this.authAct.uid,
			url:url,
			srt: [],
			user: {
				full_name:this.userL.user.full_name,
				id:this.userL.user.id,
				photo_url:this.userL.user.photo_url,
			},
			created_at: new Date(),

		};
		this.afs.collection('videos').doc(uid).set(video).then(()=> {}).catch();

	}


	videosStories:Array<any>= [];
	videoListen:any;
	verVideos(){

		return new Promise (  (resolve, rejected) =>{			
			if (this.videoListen) { this.videoListen.unsubscribe()}
			this.videoListen = this.afs.collection('videos').snapshotChanges().subscribe( data =>
				{
					this.videosStories= [];
					for (let index = 0; index < data.length; index++) {
						const element = data[index];
						this.videosStories.push(element.payload.doc.data());
						
					}
					resolve(this.videosStories);
				});
			});


	}
}
