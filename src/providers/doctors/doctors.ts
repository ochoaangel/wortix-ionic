import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppContants } from '../../app/app.constants'
import { UtilProvider} from '../util/util';
import { map } from 'rxjs/operators';
import { AuthProvider} from '../auth/auth';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';

/*
  Generated class for the DoctorsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DoctorsProvider {
	

  constructor(private afs: AngularFirestore, public http: HttpClient,  public util: UtilProvider, public auth: AuthProvider ) {
	console.log("ff");
	
   
  }
  
	doctorToshare(doctor:any){
		return {
			country_iso: doctor.country_iso,
			country_name:doctor.country_name,
			email: doctor.email,
			full_name: doctor.full_name,
			id: doctor.id,
			name: doctor.name,
			photo_url: doctor.photo_url
		}
		
	}

	transformDoctor(doctor:any){
		console.log(doctor);
		 doctor.typeO = 'doctor';
		return doctor;
	}
	
	transformDoctors(doctors:any){
		for (let i = 0; i < doctors.length; i++){
				doctors[i] = this.transformDoctor(doctors[i]);
		}
		return doctors;
	}

	doctors:any;
	doctorPromise:any;
	get_doctor_specialite(id:number,type?:string, url?:string){
		let tipo = type?"&type="+type:"";
		if (url === undefined){
			 url = AppContants.url_backend +  "api/doctors?company=222&specialty="+id+"&limit=3"+tipo;
		}else{
			url = url+ "&specialty="+id+"&limit=3"+tipo;
		}
		this.util.load(1);
		
		this.doctorPromise=  this.http.get(url).toPromise()
		this.doctorPromise.then(
		  (data:any) =>{
			this.doctors =data;
			for (let i = 0; i< data.length; i++){
				this.doctors[i].typeO = 'doctor';
				
			}
			this.util.unload(1);
		}).catch(err => {
			console.error(err);
			this.util.unload(1);
		});
		return this.doctorPromise;
	}
	

	get_doctor_search(keyword:string,type?:string,  url?:string){
	
		let search_key=keyword!=""?"&full_name="+keyword/*+"&email="+keyword*/:"";
		let tipo = type?"&type="+type:"";
		if (url === undefined){
			url =  AppContants.url_backend +"api/searchers/doctors?limit=25&sort=fullName"+search_key+tipo;
		}else{
			url = url+ "&limit=25&sort=fullName"+search_key+tipo;
		}
		this.util.load(1);
		this.doctorPromise=  this.http.get(url).toPromise()
		this.doctorPromise.then(
		  (data:any) =>{
			  this.util.unload(1);
			this.doctors =data;
			for (let i = 0; i < data.length; i++){
				this.doctors[i].typeO = 'doctor';
				
			}
		}).catch(err => {
			this.util.unload(1);
			console.error(err);
			this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los doctores" ,"bottom",'Ok',2000);
		});
		return this.doctorPromise;
	}
	
	
	get_doctor_one(id:number){
	
		let  url = AppContants.url_backend +  "api/doctors/"+id+"?api_token="+this.auth.userL.user.api_token;;
		this.util.load(1);
		this.doctorPromise=  this.http.get(url).toPromise();
		this.doctorPromise.then(
		  (data:any) =>{
			this.doctors =data;
			for (let i = 0; i < data.length; i++){
				this.doctors[i].typeO = 'doctor';
				
			}
			this.util.unload(1);
		}).catch(err => {
			console.error(err);
			this.util.unload(1);
			this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los doctores" ,"bottom",'Ok',2000);
		});
		return this.doctorPromise;
	}
	
	public get_like(doctor_id:string){
	    let url =  AppContants.url_backend +"api/doctors/"+doctor_id+"/liking" ;
		
		let doctorPromise=  this.http.get(url).toPromise()
	    doctorPromise.then(
		  (data:any) =>{
			console.log(data);
		}).catch(err => {
			console.error(err);
				this.util.showToast("Ha ocurrido un error mientras se intetaba obtenerlos likes" ,"bottom",'Ok',2000)
			
		});
		return doctorPromise;
		
	}
	
	
	public agendarCita(cita:any){
		let uid = this.afs.createId();
		if (!cita["uid"]  )
			cita["uid"] = uid;
		this.afs.collection('users').doc(this.auth.authAct.uid).collection('doctors').doc(cita.doctor.id+"").collection('citas').doc(uid).set(cita).then( () => {
			let chat = {
				cant_messages: 0,
				doctor:{
					id: cita.doctor.id,
					name: cita.doctor.full_name,
					picture: cita.doctor.photo_url,
					uid: ""
				},
				cita:{
					id: cita.id,
					create_at: cita.create_at,
					description: cita.description,
					fechaHora: cita.fechaHora,
					specialty: cita.specialty,
					title: cita.title
				},
				issueID: uid,
				last_message: {},
				patient:{
					id: this.auth.userL.user.id,
					name: this.auth.userL.user.name,
					picture: this.auth.userL.user.photo_url,
					uid: this.auth.authAct.uid
				},
				
				
			}
			this.afs.collection('chatsRooms').doc(uid).set(chat).then( () =>{
					
			})	
		});
	}
	
	
	
	/* CHAT AND VIDEO CALLS*/
	
	chatsRooms:any = [];
	chatsRoomsMessages:any = [];
	public getMyChatMessages(){
		setTimeout( () => {
			let field = 'patient.uid';
			let key =  this.auth.authAct.uid;
			if (this.auth.userL.user.role ==  'doctor'){
				field = 'doctor.id';
				key  = this.auth.userL.user.id;
			}
			this.util.load(1);
			this.afs.collection('chatsRooms', ref => ref.where(field, '==', key )).valueChanges({idField:'key'}).subscribe(
				chatsRooms => {
					console.log(	chatsRooms );
					this.chatsRooms = chatsRooms;
					this.util.unload(1);	
				}
			)
		},4000)
	}
	
	public getChatRoomMessages(room_uid:string){
		setTimeout( () => {
			this.util.load(1);
			this.afs.collection('chatsRooms').doc(room_uid).collection('msj', ref => ref.orderBy('timestamp')).valueChanges().subscribe(
				msj => {
					this.chatsRoomsMessages[room_uid] = msj;
					this.util.unload(1);	
				}
			)
		},4000);
	}
	
	public sendChatRoomMessages(room_uid:string, text: string){
		let uid = this.afs.createId();
		let msj={
			timestamp: new Date(),
			type: "text",
			value: text,
			userUID: this.auth.authAct.uid
		}
		this.util.load(1);
		this.afs.collection('chatsRooms').doc(room_uid).collection('msj').doc(uid).set(msj).then( () =>{
				this.util.unload(1);	
		})
	}
	
	
	
	public setFavoritos(doctor:any, fav?:boolean){
		//let uid = this.afs.createId();
		doctor.favorito = fav!== undefined?fav:true;
		this.afs.collection('users').doc(this.auth.authAct.uid).collection('doctors').doc(doctor.id+"").update(doctor).then( () => {
			//alert("actualizo");
		}).catch( e=> {
			this.afs.collection('users').doc(this.auth.authAct.uid).collection('doctors').doc(doctor.id+"").set(doctor).then( () => {
				//alert("actualizo");
			})
			
		});
	}

	
	favoriteDoctors:any = [];
	favoriteMapDoctors:any = new Map;
	v:any;
	public MisFavoritos(){
		//let uid = this.afs.createId();
		if (this.v === undefined)
			this.auth.authReady().then( log => {
				this.v= this.afs.collection('users').doc(this.auth.authAct.uid).collection('doctors', ref => ref.where('favorito', '==', true))
				this.v.stateChanges().subscribe( doctors => {
					for (let i =0; i< doctors.length; i++){
						let doctor = doctors[i].payload.doc.data();
						if (doctors[i].type == "added"){
							
							this.favoriteDoctors.push(doctor);
							this.favoriteMapDoctors.set(doctor.id, doctor)
						}
						
						if (doctors[i].type =="removed"){
							this.favoriteMapDoctors.delete(doctor.id)
						}
					}
				})
			})
	}
	
	
		
	
	
}

