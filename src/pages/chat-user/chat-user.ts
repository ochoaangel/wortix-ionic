import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DoctorsProvider } from '../../providers/doctors/doctors';
import { AuthProvider } from '../../providers/auth/auth';


/**
 * Generated class for the ChatUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat-user',
  templateUrl: 'chat-user.html',
})
export class ChatUserPage {
	
  
  role:string;
	role2:string;
  room:any={}
  
  texto:string="";

  constructor(public navCtrl: NavController, public navParams: NavParams, public doctorsProvider: DoctorsProvider, public auth:AuthProvider) {
  }

  ionViewDidLoad() {
    this.room = this.navParams.get('room') || JSON.parse(localStorage.room) || {};
	localStorage.room = JSON.stringify(this.room);
	console.log(this.room)
	this.doctorsProvider.getChatRoomMessages(this.room.key);
	 this.role = this.auth.userL.user.role;
    this.role2 =  this.role=='doctor'?'patient':'doctor';
	
  }
  
  sendMSJ(){
	  
	this.doctorsProvider.sendChatRoomMessages(this.room.key, this.texto);  
	this.texto = "";
  }

}
