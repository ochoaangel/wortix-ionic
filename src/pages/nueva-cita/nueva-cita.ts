import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IssuesProvider } from '../../providers/issues/issues';
import { LocationProvider } from '../../providers/location/location';
import { SpecialiteProvider } from '../../providers/specialite/specialite'
import { UtilProvider } from '../../providers/util/util'

/**
 * Generated class for the NuevaCitaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nueva-cita',
  templateUrl: 'nueva-cita.html',
})
export class NuevaCitaPage {

  ncita:any={};
  doctor:any;
  tipo:any;
  categories:any=[];
  tipos = [ {},
	{name: 'Online'},
	{name: 'Consulting Room'},
	{name: 'House Visit'}
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams, public util: UtilProvider, public issuesProvider:IssuesProvider, public locProv: LocationProvider, public providerSpacialite: SpecialiteProvider) {
	this.doctor = this.navParams.get('doctor') || JSON.parse(localStorage.doctor) || {};
	localStorage.doctor = JSON.stringify(this.doctor);
	
	this.tipo = this.navParams.get('tipo') || JSON.parse(localStorage.tipo) || {};
	localStorage.tipo = JSON.stringify(this.tipo);
	
	
	
	this.doctor.tipos = {};
	if ( this.doctor.types)
		for(let i = 0; i  < this.doctor.types.length; i++){
			this.doctor.tipos[this.doctor.types[i].id] = this.doctor.types[i]; 
			
			
		}	
	this.ncita.assign_to = this.doctor.id;
	
	if (this.tipo ==  3){
		let loc = this.locProv.pinBluePosition();
		this.ncita.latitude = loc.lat;
		this.ncita.longitude = loc.lng;
		//alert(JSON.stringify(loc));
		if (loc.lat == 0 && loc.lng == 0){
			this.util.showToast("Debe colocar la posicion donde desea ser atendido" ,"bottom",'Ok',5000)
		}
		
	}
	
	this.providerSpacialite.get_all().then( data => {
				this.categories = this.providerSpacialite.specialties;
				
			}
		);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevaCitaPage');
  }
  
  nueva_cita(){
	//  
	  this.navCtrl.push("PaymentResumePage", {doctor: this.doctor, tipo:this.tipo, cita: this.ncita, files: this.files});
  }
  
	seleccionar_ubicacion(){
	  this.navCtrl.push("CitaubicacionPage", {doctor: this.doctor, lat: this.ncita.latitude, lng: this.ncita.longitude});
	}
  
  
  
	files: Array<File>=[];
	changeListener($event) : void {
		this.files = $event.target.files;
	}

}
