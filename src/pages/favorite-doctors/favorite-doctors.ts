import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DoctorsProvider } from '../../providers/doctors/doctors';

/**
 * Generated class for the FavoriteDoctorsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-favorite-doctors',
  templateUrl: 'favorite-doctors.html',
})
export class FavoriteDoctorsPage {

	doctors:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public providerDoctors: DoctorsProvider,) {
  }

  ionViewDidLoad() {
    this.loadDoctor(12);
	this.providerDoctors.MisFavoritos();
  }
  
  
   //DOCTORS
  loadDoctors=false;
  meta:any;
  
  links:any;
  loadDoctor(id:number){
		this.doctors = [];
		this.loadDoctors = true;
	     this.providerDoctors.get_doctor_specialite(id, "online").then( data=>{
				this.doctors = data.data;
				if (this.doctors.length == 2){
					this.doctors.unshift({noDoctor:true});
				}
				this.loadDoctors = false;
				this.meta = data.meta;
				this.links = data.links;
		})
  }
  
	goToProfile(doctor:any, type: string){
		this.navCtrl.push('ProfilePage',{doctor:doctor, type: type});
	}
	
}
