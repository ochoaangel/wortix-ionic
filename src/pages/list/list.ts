import { Component, ViewChild} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
     @ViewChild('barCanvasImc') barCanvasImc;
   A:any;
   B:any;
   C:any;
   D:any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
		
	}
	
	act_chart( element, label, labelx, data ){
	
		let DataE1 = [];
		let DataE2 = [];
		let DataE3 = [];
		let DataE4 = [];
		
		for (let i =0; i < data.length;i ++){
			DataE1.push(18.5 - 15);
			DataE2.push(25 - 15);
			DataE3.push(29 - 15);
			DataE4.push(36 - 15);
			
		}
		
		 let barCanvas = new Chart(element, {
				  type: 'line',
				  data: {
				  labels: labelx,
				  datasets: [{
					  label: [label],
					  data:data,
					  backgroundColor: 'rgba(54, 162, 235, 1)',
					  borderColor: 'rgba(54, 162, 235, 1)',
					  borderWidth: 1,
					  fill: false,
				  },
				  
				  
				  {
					  label: [label],
					  data:DataE1,
					  backgroundColor: 'rgba(255, 224, 149, 0.7)',
					  borderColor: 'rgba(255, 224, 149, 1)',
					  borderWidth: 1,
					 fill: 'origin',
				  },
				  
				  {
					  label: [label],
					  data:DataE2,
					  backgroundColor: 'rgba(180, 235, 154, 0.7)',
					  borderColor: 'rgba(180, 235, 154, 1)',
					  borderWidth: 1,
					  fill: '-1',
				  },
				  
				  {
					  label: [label],
					  data:DataE3,
					  backgroundColor: 'rgb(244, 181, 130, 0.7)',
					  borderColor: 'rgba(244, 181, 130, 1)',
					  borderWidth: 1,
					  fill: '-1',
				  },
				  
				  {
					  label: [label],
					  data:DataE4,
					  backgroundColor: 'rgba(255, 155, 183, 0.7)',
					  borderColor: 'rgba(255, 155, 183, 1)',
					  borderWidth: 1,
					  fill: '-1',
				  }
				  
				  ]
			  },
			  options: {
				   title: {
					display: false,
					},
					 legend: {
						display: false,
				},
				  scales: {
					  yAxes: [{
						  ticks: {
							  beginAtZero:true,
							   // Include a dollar sign in the ticks
							callback: function(value, index, values) {
								return  parseInt(value) + 15 + "%";
							}
						  }
						  
						  
					  }]
				  },
				  
				  
				  tooltips: {
					callbacks: {
						label: function(tooltipItem, data) {
							var label = data.datasets[tooltipItem.datasetIndex].label || '';
							console.log(tooltipItem, data);
							return parseInt(tooltipItem.value)+15;
						}
					}
				}
				}
			});	
			 
		
		
		setTimeout(()=>  {
			for(let i = 1; i <  barCanvas.data.datasets.length; i++){
				let auxB=barCanvas.data.datasets[i];
				
				if (auxB._meta[0] && auxB._meta[0].data.length> 0 && auxB._meta[0].data[0]._model){
					auxB =auxB._meta[0].data[0]._model;
					if (i == 1){
						console.log(auxB);
						this.A ={
							top: parseInt(auxB.y)*0.97+"px",
							display: 'block'
						}
						console.log(this.A, auxB );
					}
					
					
				}
				
			}
		barCanvas.resize()},1000);	
		return barCanvas;
	}

    ionViewDidLoad(){
		this.act_chart( this.barCanvasImc.nativeElement, 'imc', ['A', 'B', 'C', 'D'], [26 -15,21-15,21-15,23-15] );
	}
	
	 goToProfile(doctor:any, type: string){
		this.navCtrl.push('ProfilePage',{doctor:doctor, type: type});
	 }
}
