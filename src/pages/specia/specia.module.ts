import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeciaPage } from './specia';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SpeciaPage,
  ],
  imports: [
    IonicPageModule.forChild(SpeciaPage), TranslateModule,
		ComponentsModule,
  ],
})
export class SpeciaPageModule {}
