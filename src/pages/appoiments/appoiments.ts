import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AppoimentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-appoiments',
  templateUrl: 'appoiments.html',
})
export class AppoimentsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppoimentsPage');
  }

	clinics() {
					this.navCtrl.push("ListsPage", {tipo: 2, barra:false});
				}
	
	doctors() {
					this.navCtrl.push("ListsPage", {tipo: 1, barra:false});
				}

}
