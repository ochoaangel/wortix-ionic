import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { SpecialitiesPage } from './specialities';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SpecialitiesPage,
  ],
  imports: [
    IonicPageModule.forChild(SpecialitiesPage), TranslateModule,
		ComponentsModule,
  ],
})
export class SpecialitiesPageModule {}
