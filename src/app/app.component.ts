import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events , NavController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {UtilProvider} from '../providers/util/util';
import {AuthProvider} from '../providers/auth/auth';
import { HomePage } from '../pages/home/home';
import { VideoCallPage } from '../pages/video-call/video-call';
import { ListPage } from '../pages/list/list';
import { TranslateService } from '@ngx-translate/core';

//import { UserAgent } from '@ionic-native/user-agent/ngx';
declare var iosrtc;
declare var apiRTC;
declare var apiCC;

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  webRTCClient:any;
  pages: Array<{title: string, component: any}>;

  constructor(public translate: TranslateService, public events: Events,  public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public authProvider: AuthProvider, public utilProvider: UtilProvider) {
    // this.userAgent.set('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36')
	  // .then((res: any) => console.log(res))
    // .catch((error: any) => console.error(error));
    this.sessionReadyHandler = this.sessionReadyHandler.bind(this);
    this.incomingCallHandler = this.incomingCallHandler.bind(this);
    console.log("???");

	this.translate.setDefaultLang('en');
	this.initializeApp();
	


    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
    //  { title: 'List', component: ListPage }
    ];

  }


  sessionReadyHandler(e) {
    ///   console.log("sessionReadyHandler");
       apiRTC.addEventListener("incomingCall", this.incomingCallHandler);
      /* apiRTC.addEventListener("userMediaError", this.userMediaErrorHandler);
       apiRTC.addEventListener("remoteStreamAdded", this.remoteStreamAddedHandler);
       apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler);
       apiRTC.addEventListener("hangup", this.hangupHandler);*/
       console.log(apiCC.session.apiCCId);
       this.webRTCClient = apiCC.session.createWebRTCClient({});
       /*this.infoLabel = "Your local ID : "+apiCC.session.apiCCId;*/
  }

  incomingCallHandler(e) {
   // alert("incomingCallHandler");
    //this.nav.setRoot("VideoCallPage");

   /* this.state = STATE_INCALL;
    this.buttonColor = COLOR_HANGOUT;
    this.buttonLabel = LABEL_HANGOUT;
    setTimeout(this.refreshVideoView,2000);*/
  }

  initializeApp() {
    this.platform.ready().then(() => {
      apiRTC.init({
        onReady: this.sessionReadyHandler,
        apiKey: "90c8c23592c4e936277d44f1138c7347"
      });
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
     
	  //@ts-ignore
	  // window.handleOpenURL = function (url) {
		// alert('receive URL2 ' + url);
		// };
	    this.authProvider.userObserver();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  
  cerrar_session(){
	  this.authProvider.signOut();
	  
  }
  
  reviewUser(){
	this.authProvider.reviewUser().then( data => { 
		if (data == false){
			alert("Debe iniciar sesión para disfrutar del servicio")
		}else{
			this.events.publish('myProfile');
		}
	})	
  }
}
