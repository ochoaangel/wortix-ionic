import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IssuesProvider } from '../../providers/issues/issues';

/**
 * Generated class for the AntecedantsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-antecedants',
  templateUrl: 'antecedants.html',
})
export class AntecedantsPage {
	nalergia:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams, public issuesProvider:IssuesProvider ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AntecedantsPage');
  }

	nueva_antecendente(){
	  this.issuesProvider.nueva_antecendente(this.nalergia);
	
	}
}

