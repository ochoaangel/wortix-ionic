import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ToastController} from 'ionic-angular';

/*
  Generated class for the UtilProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilProvider {

	footer:boolean=false;
	loadData:number=0;
  constructor(public http: HttpClient, public toastCtrl:ToastController) {
    console.log('Hello UtilProvider Provider');
  }
  
  load(cant: number){
	  this.loadData += cant;
	  
  }
  
  unload(cant: number){
	  this.loadData -= cant;
	  this.loadData =  this.loadData <= 0?0:this.loadData;
	  
  }
  
   /*muestra un toast que puede confirgurarse el tiempo, si tiene boton de confirmaciòn y si sale en la parte superior, inferior o en el centro*/
  showToast(text, position, showOk, duration) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: duration,
      position: position,
      showCloseButton: showOk,
      closeButtonText: 'OK'
    });
    toast.present();
  }
  
  fileClick(id){
	  let e =document.getElementById(id);
	  let e2=e.getElementsByClassName("text-input")[0];
	  //@ts-ignore
	  e2.click();
  }

}
