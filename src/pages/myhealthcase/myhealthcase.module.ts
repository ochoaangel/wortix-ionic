import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MyhealthcasePage } from './myhealthcase';
import { ComponentsModule } from '../../components/components.module';

import {
  /* MatAutocompleteModule,
   //MatBadgeModule,
   //MatBottomSheetModule,
   MatButtonModule,
   MatButtonToggleModule,
   MatCardModule,
   MatCheckboxModule,
   MatChipsModule,
   MatDatepickerModule,
   MatDialogModule,
   MatDividerModule,
   MatExpansionModule,
   MatGridListModule,
   MatIconModule,
   MatInputModule,
   MatListModule,
   MatMenuModule,
   MatNativeDateModule,
   MatPaginatorModule,*/
   MatProgressBarModule,
   MatProgressSpinnerModule,
  /* MatRadioModule,
   MatRippleModule,
   MatSelectModule,
   MatSidenavModule,
   MatSliderModule,
   MatSlideToggleModule,
   MatSnackBarModule,
   MatSortModule,
   MatStepperModule,
   MatTableModule,
   MatTabsModule,
   MatToolbarModule,
   MatTooltipModule,
   //MatTreeModule,
   ErrorStateMatcher,
   ShowOnDirtyErrorStateMatcher*/
 } from '@angular/material';
@NgModule({
  declarations: [
    MyhealthcasePage,

  ],
  imports: [
    IonicPageModule.forChild(MyhealthcasePage), TranslateModule, 
		ComponentsModule, MatProgressSpinnerModule
  ],
})
export class MyhealthcasePageModule {}
