import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MapaPage } from './mapa';
import { ComponentsModule } from '../../components/components.module';
import {
  MatProgressBarModule
} from '@angular/material';

@NgModule({
  declarations: [
    MapaPage,
  ],
  imports: [
    IonicPageModule.forChild(MapaPage), MatProgressBarModule, TranslateModule
  ],
})
export class MapaPageModule {}
