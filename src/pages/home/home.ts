import { Component, ViewChildren, ViewChild } from '@angular/core';
import { IonicPage, NavController , Slides, Events } from 'ionic-angular';
import { LocationProvider } from '../../providers/location/location';
import { AuthProvider } from '../../providers/auth/auth';
import {DoctorsProvider} from '../../providers/doctors/doctors';
import {UtilProvider} from '../../providers/util/util';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {



	os:any;
	i:number;
	myLang:string;
	@ViewChild(Slides) slides: Slides;
	mute:boolean= false;
	actualVideo : any;
	observerVideos: any;
	actualVideoFire:any;
	
	videos = [
				{ 
					source: "http://35.227.205.51/videos/medico_wortix_001.mp4",
					lang:[
							{ src:"http://35.227.205.51/vtt/medico_wortix_001_es.vtt", label:"Spanish", srclang:"es"},
							{ src:"http://35.227.205.51/vtt/medico_wortix_001_en.vtt", label:"English", srclang:"en"} 
						]
				},
				
				{ 
					source: "http://35.227.205.51/videos/medico_wortix_002.mp4",
					lang:[
						{ src:"http://35.227.205.51/vtt/medico_wortix_002_es.vtt", label:"Spanish", srclang:"es"},
						{ src:"http://35.227.205.51/vtt/medico_wortix_002_en.vtt", label:"English", srclang:"en"} 
					]
				},
				
				{ 
					source: "http://35.227.205.51/videos/medico_wortix_003.mp4",
					lang:[
					    { src:"http://35.227.205.51/vtt/medico_wortix_003_es.vtt", label:"Spanish", srclang:"es"},
						{ src:"http://35.227.205.51/vtt/medico_wortix_003_en.vtt", label:"English", srclang:"en"} 
					]
				},
				
				{ 
					source: "http://35.227.205.51/videos/medico_wortix_004.mp4",
					lang:[
							{ src:"http://35.227.205.51/vtt/medico_wortix_004_es.vtt", label:"Spanish", srclang:"es"},
						{ src:"http://35.227.205.51/vtt/medico_wortix_004_en.vtt", label:"English", srclang:"en"} 
					]
				}
		]
		
	
	 constructor(public events: Events, public navCtrl: NavController, public utilProvider: UtilProvider,  public serviceAuth: AuthProvider,public providerDoctor:DoctorsProvider) {
		this.myLang =  window.navigator.language;
		
	 }

	 ionViewWillEnter(){
		this.serviceAuth.verVideos().then( data => {
			setTimeout(() =>{
				console.log(this.slides);
				//@ts-ignore
				let video = this.slides._slides[0].firstChild.firstElementChild.firstElementChild.firstElementChild;
				try{
					video.play();
					video.muted=this.mute;
					this.actualVideo = video;
					this.actualVideoFire= this.serviceAuth.videosStories[0];
				}catch(e){
					console.error(e)
				}}, 1000)		}
		);
		this.utilProvider.footer = true;
		
		//this.showButtons();
		
		this.events.subscribe('myProfile', () => {
			this.navCtrl.push('MyprofilePage');
		});
		 this.rand();
	 }
	 
	  ionViewWillLeave(){
		this.utilProvider.footer = false;
		
		let currentIndex = this.slides.getActiveIndex();
		if( currentIndex > 0){
			//@ts-ignore
			let videoAnt = this.slides._slides[currentIndex-1].firstChild.firstElementChild.firstElementChild.firstElementChild;
			videoAnt.currentTime = '0';
			videoAnt.pause();
		}
		
		if( currentIndex < this.slides._slides.length - 1){
			//@ts-ignore
			let videoNext = this.slides._slides[currentIndex+1].firstChild.firstElementChild.firstElementChild.firstElementChild;
			videoNext.currentTime = '0';
			videoNext.pause();
		}
		//@ts-ignore
		let video = this.slides._slides[currentIndex].firstChild.firstElementChild.firstElementChild.firstElementChild;
		video.currentTime = '0';
		video.pause();
		this.actualVideo = video;
		this.actualVideoFire= this.serviceAuth.videosStories[currentIndex];
		this.events.unsubscribe('myProfile');
	//	this.observerVideos.unsubscribe();

	 }
	 
	 goTo(path:string){
		this.navCtrl.push(path);
	 }
	 
	 
	settingBotons:boolean=true;
	timeOutshowButtons:any;
	showButtons(){
		this.settingBotons = true;
		this.utilProvider.footer = true;
		clearTimeout(this.timeOutshowButtons);
		this.timeOutshowButtons = setTimeout(()=>{
			this.utilProvider.footer = false;
			this.settingBotons = false;
		},5000)
	}
	 
	next() {
		this.rand();
		let currentIndex = this.slides.getActiveIndex();
		if (currentIndex >= this.slides._slides.length || currentIndex < 0){
			return;
		}
		if( currentIndex > 0){
			//@ts-ignore
			let videoAnt = this.slides._slides[currentIndex-1].firstChild.firstElementChild.firstElementChild.firstElementChild;
			if (videoAnt){
				try{
					videoAnt.currentTime = '0';
					videoAnt.pause();
				}catch(e){
					console.error(e)
				}
			}
			
		}
		
		if( currentIndex < this.slides._slides.length - 1){
			//@ts-ignore
			let videoNext = this.slides._slides[currentIndex+1].firstChild.firstElementChild.firstElementChild.firstElementChild;
			if (videoNext){
				try{
					videoNext.currentTime = '0';
					videoNext.pause();
				}catch(e){
					console.error(e)
				}
			}
		}
		//@ts-ignore
		let video = this.slides._slides[currentIndex].firstChild.firstElementChild.firstElementChild.firstElementChild;
		try{
			video.play();
			video.muted=this.mute;
			this.actualVideo = video;
			this.actualVideoFire= this.serviceAuth.videosStories[currentIndex];
			console.log(this.actualVideoFire);
		}catch(e){
			console.error(e)
		}
	}
	
	reviewUser(){
			//alert(2324);
			this.serviceAuth.reviewUser().then( data => { 
				if (data == false){
					alert("Debe iniciar sesión para disfrutar del servicio")
				}else{
				
					this.navCtrl.push('MyprofilePage');
				}
			})	
			;
	  
  }
  
  reviewUserDoctor(){
	  alert(55);
	this.navCtrl.push('AcountPage', {type: 'registerDoctor'});
	  
  }
  
  mute_v(){
	  this.mute = !this.mute;
	  this.actualVideo.muted = this.mute;
	  
  }
  picture:any="1";
  rand(){
	  this.picture= Math.floor(Math.random() * 20) + 1  ;
  }


  goToDoctor(doctor_id){
	this.providerDoctor.get_doctor_one(doctor_id).then( doctor=>{
			console.log( doctor.data[0]);
			this.navCtrl.push('ProfilePage',{doctor: doctor.data[0], type: 'doctor'});
		}
	)
	}
	
	callPage(){
		this.navCtrl.push('VideoCallPage',{});

	}
}
  

