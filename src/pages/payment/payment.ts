import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PagoProvider } from '../../providers/pago/pago';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
	 doctor:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pagoProvider: PagoProvider) {
  }

  ionViewDidLoad() {
	this.doctor = this.navParams.get('doctor') || JSON.parse(localStorage.doctor) || {};
	localStorage.doctor = JSON.stringify(this.doctor);
	console.log(this.doctor);
	this.doctor.tipos = {};
	for(let i = 0; i  < this.doctor.types.length; i++){
		this.doctor.tipos[this.doctor.types[i].id] = this.doctor.types[i]; 
	}
	console.log(this.doctor);
  }
  
  paypal(){
	  this.pagoProvider.pagarPaypal(50, "primero");
	  
  }

}
