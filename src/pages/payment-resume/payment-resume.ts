import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { PagoProvider } from '../../providers/pago/pago';
import { DoctorsProvider } from '../../providers/doctors/doctors';
import { IssuesProvider } from '../../providers/issues/issues';


/**
 * Generated class for the PaymentResumePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-resume',
  templateUrl: 'payment-resume.html',
})
export class PaymentResumePage {
	
  doctor:any;
  tipo:any;
  tipos = [
	{name: 'Online', value: 'online'},
	{name: 'Consulting Room', value: 'consultation-room'},
	{name: 'House Visit', value: 'house-visit'}
  ]
  cita:any;
  files: Array<File> =[];
  constructor(public navCtrl: NavController, 
				public navParams: NavParams,
				public authService:AuthProvider,
				public pagoProvider: PagoProvider,
				public providerDoctor: DoctorsProvider,  
				public issuesProvider:IssuesProvider) {
					
	this.doctor = this.navParams.get('doctor') || JSON.parse(localStorage.doctor) || {};
	localStorage.doctor = JSON.stringify(this.doctor);

	this.tipo = this.navParams.get('tipo') || JSON.parse(localStorage.tipo) || {};
	localStorage.tipo = JSON.stringify(this.tipo);
	
	this.cita = this.navParams.get('cita') || JSON.parse(localStorage.cita) || {};
	localStorage.cita = JSON.stringify(this.cita);
	
	this.files = this.navParams.get('files') || JSON.parse(localStorage.files) || [];
	localStorage.files = JSON.stringify(this.files);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentResumePage');
  }

  paypal(){
	  this.pagoProvider.pagarPaypal(25, "primero").then( data=>
	  {
		  if (data == true){
			  let cita = {
				  tipo: this.tipo,
				  create_at: new Date(),
				  fechaHora: new Date(),
				  doctor: this.providerDoctor.doctorToshare(this.doctor)
			  }
			  this.cita.ref_payment = "paypal";
			  this.cita.method_payment = "paypal";
			  this.cita.type = this.tipos[this.tipo].value;
			  this.cita.company_id = 222;
			
			  this.issuesProvider.nueva_cita(this.cita, this.files).then( data => {
					  Object.assign(cita, data.data);
					 this.providerDoctor.agendarCita(cita)
				  
				}
				
			  
			  )
			 
			  
			  this.navCtrl.setRoot("PaymentConfirmPage", {doctor: this.doctor});
		  }else{
			  alert("Debe hacer el pago");
		  }
		  
	  }
	  
	  
	  );
	  
  }
}
