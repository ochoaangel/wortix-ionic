import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentResumePage } from './payment-resume';

@NgModule({
  declarations: [
    PaymentResumePage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentResumePage), TranslateModule,
  ],
})
export class PaymentResumePageModule {}
