import { NgModule } from '@angular/core'; 
import { IonicPageModule } from 'ionic-angular';
import { SerchPrincipalPage } from './serch-principal';
import { ComponentsModule } from '../../components/components.module';
import { TranslateModule } from '@ngx-translate/core'

@NgModule({
  declarations: [
    SerchPrincipalPage,
  ],
  imports: [
    IonicPageModule.forChild(SerchPrincipalPage), TranslateModule,
	ComponentsModule,
	TranslateModule
  ],
})
export class SerchPrincipalPageModule {}
