import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, ApplicationRef, NgZone} from '@angular/core';

import { AppContants } from '../../app/app.constants'


import {GoogleMaps,GoogleMap,GoogleMapsEvent,GoogleMapOptions,CameraPosition,MarkerOptions,Marker,Environment,LocationService,
		MyLocationOptions,ILatLng,Geocoder,GeocoderRequest,GeocoderResult,MyLocation, GoogleMapsMapTypeId, HtmlInfoWindow } from '@ionic-native/google-maps';

import { LoadingController,AlertController,Platform ,MenuController,NavController,Events,ToastController } from 'ionic-angular';
import { UtilProvider} from '../util/util';



/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {
  	map: GoogleMap=null;  	//MAPA ORIGINAL
	platformReady:boolean=true; //PARA SABER SI LA PLATAFORMA ESTA CARGADA
	datosMapa:any={};			// DATOS DEL MAPA
	km:number=36;				// DISTANCIA DE BUSQUEDA
	noRefresh:boolean=false;	// Controla la actualizacion de datos para que no sea constate
	noSearch:boolean=false;		//Gestiona que no se realicen busquedas inncesarias
	haveCircle:boolean=false;		//Gestiona que no se realicen busquedas inncesarias
	moveToUserPin:boolean=false;		//Gestiona que no se realicen busquedas inncesarias
	cameraMoveOn:boolean=false;	//Aceptar los efectos de mover camara
	PID1:number=0;				//PID codigo RAMDOM de busqeuda
  
	promiseMap:any //PROMEASA que indica si el mapa esta listo
	
	
  	constructor(private platform: Platform, private zone: NgZone, private googleMaps: GoogleMaps, public events: Events,private chRef: ApplicationRef, public http: HttpClient, public util: UtilProvider) {
    	 
	}
	
	
	/*Funcion que crea el mapa por primera vez si no existe
		Resuelve: true cuando el mapa esta disponible
	*/
	iniciarMapa(){
		this.promiseMap = new Promise ( (resolve, rejected) =>{
			this.platform.ready().then(() => {
			
				if (this.platform.is('cordova')){
					Environment.setEnv({
					   'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBn4uF_5Iy_ikPMD7iMk6xxm7d-OzOFGGw',
					   'API_KEY_FOR_BROWSER_DEBUG' : 'AIzaSyBn4uF_5Iy_ikPMD7iMk6xxm7d-OzOFGGw'
					 });
				}
				this.getPosicionUser().then(data => {
					this.actualizarPosionMapa(data.latitud,data.longitud);
					this.platformReady=true;
					if(this.map==null){
						this.loadMap(false).then(data => resolve(data));
					}else{
						resolve(true);
					}
				});
			});
		})
		return this.promiseMap;
	}
	clearMarkers(){
		if (this.datosMapa.markers && this.datosMapa.markers.length > 0){
			console.log(this.datosMapa.markers);
			for (let i = 0; i  < this.datosMapa.markers.length; i++){
				
				this.datosMapa.markers[i].remove(); 
			}
		}
		this.datosMapa.markers = [];
	}
	
	clearMap(){
		this.clearMarkers();
		//@ts-ignore
		cerrar();
		
	}
	dataActual:any={
		data : {
			doctors:{}
		}
		
	};
	
	
	generateIWD(us:any){
		
		let resp= '<div class="div-1" style="display: inline-flex;">'+
				'	<div >'+
				'		<img src="'+us.photo_url+'" style="">'+
				'	</div>'+
				'	<div >'+
				'		<h2 style="font-size:1.4rem; margin-top:2px">'+us.name +' '+us.last_name+'</h2>'+
				'		<p class="info-p">'+ (us.specialty?us.specialty.name:'')+'</p>'+
				(us.country_iso?'		<p class="info-p" > <span class="flag-icon flag-icon-'+us.country_iso.toLowerCase()+'" style=" opacity: 0.65;"></span>'+ us.country_iso +'</p>':"")+
				'<p class="info-p">';
				
				for (let i = 0; i <5; i++){
					resp += (us.rating >= i)? '<ion-icon class="ion-md-star" style="color:#ee9e02" ></ion-icon>': '<ion-icon class="ion-md-star-outline" style="color:#ee9e02" ></ion-icon>'
				
				}
				
				resp+='	</p>  <button onclick="fmap(1)"> Solicitar</button> <button  onclick="fmap(2)"> Ver Perfil</button> <button  onclick="cerrar()"> Cerrar</button> </div> </div>'+
				'</div>';
		return resp;
		
	}
	typeActual:string='doctors';
	
	
	infoWindow: any;
	htmlInfoWindow:any;				 
	putMarkets(){
		
		let  data = this.dataActual;
		let type = this.typeActual;
		//console.log(type, data);
	//	console.log(data.data[type][i]);
		this.clearMarkers();
		//alert("update doctors");
		 for (let i =0; i < data.data[type].length; i++){
			let auxDoctor = data.data[type][i];
			 let mkOpt = this.generateMarket(auxDoctor.latitude,  auxDoctor.longitude);
			 //alert(auxDoctor.latitude +" "+  auxDoctor.longitude);
			 
			 this.datosMapa.markers = [];
			 this.map.addMarker(mkOpt).then((marker:Marker) => {
					this.datosMapa.markers.push(marker);
					marker['Doctor'] =  auxDoctor;
					//marker.setSnippet(this.generateIWD(auxDoctor));
					
					auxDoctor.moveTome =  () => {
						 this.noRefresh=true;
						 //@ts-ignore
						 fmap = (tipo) => {
							 //@ts-ignore
								this.htmlInfoWindow.close()
							console.log("fmap")
							auxDoctor.selfClick(tipo);
						}	
						
						this.map.animateCamera({
								target: {lat: parseFloat( auxDoctor.latitude), lng:  parseFloat(auxDoctor.longitude)},
								 duration: 200
							});
							
						if (this.map['selectMarket'] != null){
								this.map['selectMarket'].setIcon( 'assets/imgs/pin-doc.png' );
								this.map['selectMarket'].setZIndex(0);
								this.map['selectMarket']['Doctor'].bandera = false;

						}	
						let m:any =marker;
						try{ 
							this.htmlInfoWindow = new HtmlInfoWindow();

							let frame: HTMLElement = document.createElement('div');
							frame.innerHTML = [
								this.generateIWD(auxDoctor)
							].join("");
							
							this.htmlInfoWindow.setContent(frame, {
							  width: "240px",
							 'border-radius': "10px"
							 
							});

							this.htmlInfoWindow.open(marker);
							//@ts-ignore
							cerrar = () =>{
								//@ts-ignore
								this.htmlInfoWindow.close()
							}
						}catch(e){
							console.log(e);
						}
						
						
							let  imageIcon = {
								url:'assets/imgs/pin-doc-act.png'
							}
						m.setIcon(imageIcon);
						m.setZIndex(1000);
						this.map['selectMarket'] = marker;
						auxDoctor.bandera = true;
						//@ts-ignore
						
						
						
						if (auxDoctor.selfClick){
							//auxDoctor.selfClick()
						}
					}
					marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
						  
						auxDoctor.moveTome();
					});
					
				
				
					
			  });
			 
		 }
	
		if (this.callbackGD){
			this.callbackGD(data);
		}
	}
	
	
	callbackGD:any=null;
	lastData:any =null;
	promiseHTTP:any=null;
	specialty:any=null;
	get_doctor_by_distance(km, latitud, longitud, localPID){
		if (this.noSearch){
			return new Promise((resolve,rejected) => { resolve() });
		}
		let busqueda:any ={
				'reach': km*1000,
				'longitude': longitud,
				'latitude': latitud
		}
		if (this.specialty != null){
			busqueda.specialty_doctor = this.specialty;
		}
		//alert(44);
		if (this.lastData != null){
			//alert(595);
			if (busqueda.longitude == this.lastData.longitude && busqueda.latitude == this.lastData.latitude &&  busqueda.reach == this.lastData.reach && this.lastData.type == this.typeActual){
				//alert(555);
				return new Promise((resolve,rejected) => {
					this.promiseHTTP.then(
					  (data:any) =>{
						  //alert(4);
						   this.dataActual.data.doctors = data.doctors;
						   this.putMarkets();
						   
						   resolve();
						
					}).catch(err => {
						this.lastData = null;
						this.util.unload(1);
						console.error(JSON.stringify(err));
					});
				})
			}
		}
		
		this.lastData = busqueda;
		this.lastData.type = this.typeActual;
		//alert(JSON.stringify(busqueda))
		this.util.load(1);
		//alert(5545);
		let url =  AppContants.url_backend +"api/searchers/close-to-me/"+this.typeActual;
		return new Promise((resolve,rejected) => { 
			this.promiseHTTP = this.http.post(url, busqueda).toPromise()
			//alert(121);
			this.promiseHTTP.then(
			  (data:any) =>{
				    // alert(323);
					if (this.PID1  != 0){
						this.util.unload(1);
						this.dataActual.data[this.typeActual] = data[this.typeActual];
						this.putMarkets();
					}
					if (this.PID1  == localPID){
						this.PID1 =0;
					}
						   
				   resolve();
				
			}).catch(err => {
				this.lastData = null;
				this.util.unload(1);
				 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se hacia la busqueda" ,"bottom",'Ok',2000)
			});	
		})
	}
	
	
	generateMarket(lat, lng){
		
		let marker: MarkerOptions = {
          title: null,
          position: {lat:lat,lng: lng},
          animation:null,
          zIndex: 0,
          disableAutoPan: true,
		  icon: 'assets/imgs/pin-doc.png',
		  draggable: false
          //index:key
		
        };
		return marker;
		
		
		
	}
	
	research(){
		try {
			this.PID1 = Math.round(Math.random() *110550)+1;
			return this.get_doctor_by_distance(this.km, this.lastData.latitude, this.lastData.longitude, this.PID1);
		}catch(e){
			this.PID1 = Math.round(Math.random() *110550)+1;
			return this.get_doctor_by_distance(this.km, this.datosMapa.mk.latitude, this.datosMapa.mk.longitude, this.PID1 );
		}
	}
	
	
	userPin(lat: any,	lng:any, drag?:boolean){
		return new Promise ( (resolve, rejected) =>
		{
			if (this.datosMapa.mk){
				if (this.datosMapa.mk.circle){
					this.datosMapa.mk.circle.remove();
				}
				this.datosMapa.mk.remove();
			}
			this.datosMapa.mkOpt = this.generateMarket(lat, lng);
			this.datosMapa.mkOpt.draggable =drag?drag:true;
			this.map.addMarker(this.datosMapa.mkOpt).then((marker:Marker) => {
					//@ts-ignore					
					marker.setIcon('assets/imgs/pin-paciente.png');
					this.datosMapa.mk = marker;
					this.datosMapa.mk.on(GoogleMapsEvent.MARKER_DRAG_END).subscribe(mk => {
						this.PID1 = Math.round(Math.random() *110550)+1;
						this.get_doctor_by_distance(this.km,mk[0].lat,  mk[0].lng, this.PID1).then(a => {
							
						})
					})
					 if (this.noSearch && this.haveCircle){
						 this.putCircle();
					 }	
					resolve(this.datosMapa.mk)
											
			});
		});
			
		
		
	}
	
	pinBluePosition(){
		try{
			return {lat: this.datosMapa.mk.getPosition().lat, lng: this.datosMapa.mk.getPosition().lng}
		}catch(e){
			this.util.showToast("No se pudo obtener la ubicacion del usuario" ,"bottom",'Ok',2000);
			return {lat: 0, lng: 0}
			
		}
		
	}
	
	pinblueSTC(lat: any,	lng:any){
		this.userPin(lat,	lng, false).then(()=> {
			this.datosMapa.mk.draggable =false;
			 this.map.animateCamera({
				target: this.datosMapa.mk.getPosition(),
				duration: 200
			  });
		})
		
	}
	
	putCircle(){
		this.zone.run(() => {
			if (	this.datosMapa.mk){
					if (this.datosMapa.mk.circle){
						this.datosMapa.mk.circle.remove();
					}
			}
			let circle: any = this.map.addCircleSync({
							 center: this.datosMapa.mk.getPosition(),
							radius: 9*1604.34,
							fillColor: "rgba(0, 0, 255, 0.3)",
							strokeColor: "rgba(0, 0, 255, 0.30)",
							strokeWidth: 1
						});
						this.datosMapa.mk.circle = circle;
						this.datosMapa.mk.bindTo("position", circle, "center");
		});	
	}
	kmASet(km){
		this.datosMapa.mk.circle.setRadius((km/4)* 1609.34);
	}
	
	
	//funcion para cargar el mapa en la interfaz
	loadMap(x){
		return new Promise( (resolve,rejected) => {
			try{
				if(this.platformReady){
					if(this.map==null ){
							LocationService.getMyLocation().then((myLocation: MyLocation) => {
									let mapOptions:any  = {
										controls: {
											compass: false,
											myLocation: true,
											mapToolbar: true
										  },
										  gestures: {
											scroll: true,
											tilt: true,
											zoom: true,
											rotate: true
										  },
										  camera: {
											target: myLocation.latLng,
											zoom: 11, 
											tilt: 30
										  }
									};
									//alert(JSON.stringify(myLocation));
									
									this.map = GoogleMaps.create('map_canvas', mapOptions);
									this.map.one(GoogleMapsEvent.MAP_READY).then(() => { 
										this.map.on(GoogleMapsEvent.MY_LOCATION_BUTTON_CLICK).subscribe( data => {
										  console.log("Mi posicion", JSON.stringify(data));
										});
										
										if (this.moveToUserPin)
											this.userPin( myLocation.latLng.lat,  myLocation.latLng.lng);
										this.map.on(GoogleMapsEvent.CAMERA_MOVE_END).subscribe(data => 
											{
												if (this.cameraMoveOn){
													this.PID1 = Math.round(Math.random() *110550)+1;
													let localPID = this.PID1;
													setTimeout(()=>{
														if (this.PID1  == localPID){
															this.zone.run(() => {
																if ( this.noRefresh==false){
																	//console.log(this.map.getCameraPosition());
																	//@ts-ignore
																	this.km=this.getDistanceFromLatLonInKm(this.map.getCameraPosition().target.lat,  this.map.getCameraPosition().target.lng,  this.map.getCameraPosition().northeast.lat,  this.map.getCameraPosition().northeast.lng);
																	this.get_doctor_by_distance(this.km, this.map.getCameraPosition().target.lat,  this.map.getCameraPosition().target.lng, localPID ).then(a => {})
																}else{
																	this.noRefresh=false;
																}
															})
														}
													},500);
												}
											}
										);
										this.PID1 = Math.round(Math.random() *110550)+1;
										//@ts-ignore
										this.km=this.getDistanceFromLatLonInKm(this.map.getCameraPosition().target.lat,  this.map.getCameraPosition().target.lng,  this.map.getCameraPosition().northeast.lat,  this.map.getCameraPosition().northeast.lng);
										this.get_doctor_by_distance(this.km, this.map.getCameraPosition().target.lat,  this.map.getCameraPosition().target.lng, this.PID1).then(a => {})
									
										resolve(true);
									})
									.catch(error =>{
										//alert(3);
										//mensaje de error que diga que verifique la conexion
										//alert("hubo un error en el mapa");
										console.error(error,JSON.stringify(error));
										rejected(false);
										 this.util.unload(1);
										this.util.showToast("El mapa de google ha tenido un error critico" ,"bottom",'Ok',2000)
										//alert(error);
									});
						  })
					}else{
					   this.resetLastPosition(x);
					}
				}else{
					console.log("la plataforma aun no esta lista");
				}
			}catch(error){
				//alert(4);
				console.error(error);
				rejected(false);
			  alert(error);
			};
		})
	}
	
	/* REGRESA A LA ULTIMA POSICION*/
	resetLastPosition(x:boolean){
		 setTimeout(()=>{   
				       this.map.setDiv('map_canvas');
				       if(x){
					       let posicion={lat: this.datosMapa.latitud ,lng: this.datosMapa.longitud};
					       this.map.animateCamera({
						        target: posicion,
						        zoom: 12, 
								tilt: 30,
								duration: 200
						    });
							
				   		}
				    },1000);
		
	}
	
	 //metodo que me devuelve la posicion del usuario
	  getPosicionUser():any{
		return new Promise((resolve) =>{
			/*
			  this.nativeStorage.getItem('posicionUser').then(
				data => {
				  this.posicionUser =  JSON.parse(data);
				  resolve(this.posicionUser);
				},
				error => {
				  if(this.posicionUser!=null){
					 resolve(this.posicionUser);
				  }else{
					resolve(null);
				  }
			  }); */
			  resolve({latitud: -12.172781 , longitud:-76.934686})
		});
	  }

	
	


	//funcion para obtener la posicion actual del usuario
	getPosition(): void{
		let options: MyLocationOptions = {
		  enableHighAccuracy: true
		};
	    this.map.getMyLocation(options).then(response => {
	    this.actualizarPosionMapa(response.latLng.lat,response.latLng.lng);
	    this.map.animateCamera({
	        target: /*{lat:10.961856, lng:-63.857477}*/ response.latLng,
	        duration: 200
	      });
		  this.noRefresh=true;
	      let posicion={latitud: response.latLng.lat, longitud:response.latLng.lng /*latitud:10.961856, longitud:-63.857477*/};
	      //this.userService.savePosicionUser(posicion).then(rta => {
	    	// this.addMarkerUser(response.latLng/*{lat:10.961856, lng:-63.857477}*/);
	     // });
	    }).catch(error =>{
	    	console.log(error);
			 this.util.unload(1);
				this.util.showToast("No se pudo obtener la ubicacion del usuario" ,"bottom",'Ok',2000)
       
      });
	}
	
	actualizarPosionMapa(lat,lng){
		this.datosMapa.latitud=lat;
	    this.datosMapa.longitud=lng;
	    console.log("datosMapa",this.datosMapa);
	}

	
	resultadosQ:any=[];
	//funcion para buscar un lugar a traves del servicio de google de geocoder (llevar una direccion en texto a coordenada geografica)
	buscarEnMapa(key:string){
		this.datosMapa.buscar = key;
		if(this.datosMapa.buscar==""){
			return;
		}
	/*	if(this.cancelarPeticion()==true){
		   //console.log("esta pintando y debo hacer un return");
			return;
		}*/
		let options: GeocoderRequest = {
		   address: this.datosMapa.buscar
		 };
		 let searchPromise = Geocoder.geocode(options)
		 searchPromise.then((results: GeocoderResult[]) => {
			console.log(results);
			this.resultadosQ = results;
			if(results.length>0){
				if(results.length > 0){
					let cadena="";
			   		 for (var j = 0; j < results[0].extra.lines.length; ++j) {
			   		 	let x=", ";
			   		 	if(j==results[0].extra.lines.length-1){
			   		 		x="";
			   		 	}
			   		 	cadena+=results[0].extra.lines[j]+x;
			   		 }
			   		 let datos={position:results[0].position,sitio:cadena};
			   		 this.procesarBusqueda(datos);

                                        /* solo para ios
                                        //@ts-ignore
                                        if(results[0].extra.address.Name!=null&&results[0].extra.address.Name!=""){
                                           //@ts-ignore
                                          cadena=results[0].extra.address.Name;
                                        }
                                         //@ts-ignore
                                        if(results[0].extra.address.State!=null&&results[0].extra.address.State!=""){
                                           //@ts-ignore
                                          cadena+=", "+results[0].extra.address.State;
                                        }
                                         //@ts-ignore
                                        if(results[0].country!=null&&results[0].country!=""){
                                           //@ts-ignore
                                          cadena+=", "+results[0].country;
                                        } 
                                        let datos={position:results[0].position,sitio:cadena};
                                        this.procesarBusqueda(datos);
                                        */
				}else{
						//this.doRadio(results);
				}
			}else{
        //alert(2);
       /* this.finaliozar();
				this.datosService.loadingService.presentToast("No se encontraron resultados para su busqueda. Intente ser mas especifico");
			*/
			}
		}).catch(err => {
        //alert(3);
		
		 this.util.unload(1);
				this.util.showToast("No se ha logrado obtener ningun resultado: Por favor compruebe su conexion a internet he intentelo nuevamente" ,"bottom",'Ok',2000)
      });
	
		return searchPromise;
	}

	
	//funcion para procesar una busquedaueda
	procesarBusqueda(data){
		this.datosMapa.buscar=data.sitio;
		this.datosMapa.posPingBusqueda=data.position;
    //this.clearMap().then((respuesta) => {
			//if(respuesta==true){
				//this.centrarMapaEnPing=false;
				let target: ILatLng = data.position;
			  this.datosMapa.latitud= target.lat;
			  this.datosMapa.longitud=target.lng;
			   this.map.animateCamera({
                target: this.datosMapa.posPingBusqueda,
                duration: 200
            });
			console.log(this.datosMapa.mk);
		//	this.get_doctor_by_distance(this.km, this.datosMapa.latitud,  this.datosMapa.longitud).then(a => {
							
							
			//			})
												this.userPin(this.datosMapa.latitud,  this.datosMapa.longitud)
			
			
			
			  //this.verificarActividades();
			//}
	    //});
	}
	
	getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
		var R = 6371; // Radius of the earth in km
		var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
		var dLon = this.deg2rad(lon2-lon1); 
		var a = 
			Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
			Math.sin(dLon/2) * Math.sin(dLon/2); 
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			var d = R * c; // Distance in km
		return d;
	}

	deg2rad(deg) {
		return deg * (Math.PI/180)
	}


}
