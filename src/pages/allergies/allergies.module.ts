import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { AllergiesPage } from './allergies';

@NgModule({
  declarations: [
    AllergiesPage,
  ],
  imports: [
    IonicPageModule.forChild(AllergiesPage), TranslateModule,
  ],
})
export class AllergiesPageModule {}
