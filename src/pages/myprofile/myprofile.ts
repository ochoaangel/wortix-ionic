import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { FilesProvider } from '../../providers/files/files';
import { UtilProvider } from '../../providers/util/util';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture';


/**
 * Generated class for the MyprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-myprofile',
	templateUrl: 'myprofile.html',
})
export class MyprofilePage {

	options = [
		{
			id: 1,
			name: "Clinic History",
			ico_path: "/assets/imgs/search-p/1.png",
			ico: "pulse",
			click: () => {
				this.navCtrl.push("ClinicHistoryPage");
			}
		},

		{
			id: 2,
			name: "Favorite Doctors",
			ico_path: "/assets/imgs/search-p/2.png",
			ico: "star",
			click: () => {
				this.navCtrl.push("FavoriteDoctorsPage");
			}
		},

		{
			id: 2,
			name: "Health Case",
			ico: "wortixgeneral-medicine",
			ico_path: "/assets/imgs/search-p/4.png",
			click: () => {
				this.navCtrl.push("MyhealthcasePage", { from: 'profile' });
			}

		},

		{
			id: 2,
			name: "Sign Out ",
			ico: "log-out",
			ico_path: "/assets/imgs/search-p/4.png",
			click: () => {

				this.cerrar_session();
			}

		},


	]



	optionsDoctor = [

		{
			id: 2,
			name: "Health Case",

			ico_path: "/assets/imgs/search-p/4.png",
			ico: "wortixgeneral-medicine",
			click: () => {
				this.navCtrl.push("MyhealthcasePage", { from: 'profile' });
			}

		},

		{
			id: 3,
			name: "My Location",
			ico: "map",
			ico_path: "/assets/imgs/search-p/4.png",
			click: () => {
				this.navCtrl.push("MapaUbicationPage", { from: 'ubication-set' });
			}

		},


		{
			id: 3,
			name: "Subir Historia",
			ico: "film",
			ico_path: "/assets/imgs/search-p/4.png",
			click: () => {
				let options: CaptureVideoOptions = { limit: 1, duration:30 , quality:0};
				console.log(this.mediaCapture.supportedVideoModes);
				
					this.mediaCapture.captureVideo(options)
					.then(
						(data: MediaFile[]) => {console.log(data);
							data[0].getFormatData(data=> console.log(data), error => console.log(error));
							this.uploadImage(data[0].fullPath,data[0].name)},
						(err: CaptureError) => console.error(err)
					);

			}

		},

		{
			id: 2,
			name: "Sign Out ",
			ico: "log-out",
			ico_path: "/assets/imgs/search-p/4.png",
			click: () => {

				this.cerrar_session();
			}

		},


	]



	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				public authService: AuthProvider,
				public fileService: FilesProvider, 
				private mediaCapture: MediaCapture,
				public util:  UtilProvider) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MyprofilePage');
	}

	cerrar_session() {
		this.authService.signOut();
		this.navCtrl.pop();

	}

	addCamara() {
		this.fileService.addCameraPhoto(1, false).then(
			data => {
				alert(data);
				//this.uploadImage(data);
			}

		).then(img => {
			//this.showLoadind("dots", "cargando nueva imagen");
			
			
		}).catch(error => {
			this.util.showToast(error, 'bottom', false, 3000);
		});

	}

	

	uploadImage(url, name) {
		this.fileService.upload_image(url, this.authService.authAct.uid+"/"+Math.random()*10000 + name, "user_videos", progress => {
			console.log(parseInt(progress))
		}).then(image => {
			this.util.showToast('Video guardado','bottom', false, 2000);
			this.authService.newVideo(image);
		}).catch(err => this.util.showToast('No se pudo subir su video, por favor intentelo nuevamente. ' + err.message, 'top', false, 3000)/*alert("error c: "+JSON.stringify(err))*/);
	}


}
