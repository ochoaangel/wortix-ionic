import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private platform: Platform) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

	about(){
		
		let options = 'location=no,closebuttoncaption=X';
					if(this.platform.is('android')){options = 'location=no,hardwareback=no,footer=yes,closebuttoncaption=X,footercolor=#1B1B1B,closebuttoncolor=#488aff'}
					window.open('https://wortix.com/about-us/', '_blank', options);//, '_system');
	}
}
