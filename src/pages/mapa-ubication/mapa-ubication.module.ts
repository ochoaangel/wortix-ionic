import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MapaUbicationPage } from './mapa-ubication';

@NgModule({
  declarations: [
    MapaUbicationPage,
  ],
  imports: [
    IonicPageModule.forChild(MapaUbicationPage), TranslateModule,
  ],
})
export class MapaUbicationPageModule {}
