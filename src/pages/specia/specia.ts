import { Component, ViewChild,ElementRef, NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams , Scroll, Content } from 'ionic-angular';
import { SpecialiteProvider } from '../../providers/specialite/specialite';
import { DoctorsProvider } from '../../providers/doctors/doctors';
import { IssuesProvider } from '../../providers/issues/issues';
import { PlacesProvider } from '../../providers/places/places';


@IonicPage()
@Component({
  selector: 'page-specia',
  templateUrl: 'specia.html',
})
export class SpeciaPage {

	stars = [1,2,3,4,5];
    @ViewChild('contentCategories') contentCategories: Content;
    @ViewChild('contentDoctor') contentDoctor: Content;
    @ViewChild('contentPlaces') contentPlaces: Content;
    @ViewChild('contentIssues') contentIssues: Content;
	categories:any;
	doctors:any;
	issues:any;
	places:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
			  public providerSpacialite: SpecialiteProvider,
			  public providerDoctors: DoctorsProvider,
			  public providerIssues: IssuesProvider,
			  public providerPlaces: PlacesProvider,
			  private zone: NgZone) {
  }

  see_all(){
		this.navCtrl.push("SpecialitiesPage");
   }
   

  ionViewDidLoad() {
	this.providerSpacialite.get_all().then( data => {
		this.categories = this.providerSpacialite.specialties
		let idSp = this.providerSpacialite.specialty? this.providerSpacialite.specialty.id : 12;
		this.search(idSp);
		setTimeout(()=> {
			this.scrollTo("cat"+idSp)
			console.log(this.categories);
		},1000);
	})
	
  }
  
  search(id:number){
	this.loadDoctor(id);
	this.loadIssues(id);
	this.loadPlaces(id);
  }
  
  
	selectSpecialitie(spacialite:any){
		this.providerSpacialite.specialty =spacialite;
		this.search(spacialite.id)
	}
  //DOCTORS
  loadDoctors=false;
  meta:any;
  links:any;
  loadDoctor(id:number){
		this.doctors = [];
		this.loadDoctors = true;
	     this.providerDoctors.get_doctor_specialite(id, "online").then( data=>{
				 data.data = this.providerDoctors.transformDoctors(data.data);
				this.doctors = data.data;
				this.loadDoctors = false;
				this.meta = data.meta;
				this.links = data.links;
		})
  }
  
	moreDoctors(){
		let id= this.providerSpacialite.specialty.id;
		 this.zone.run(() => {
			this.loadDoctors = true;
			})
		 this.providerDoctors.get_doctor_specialite(id, "online",this.links.next ).then( data=>{
				
				 this.zone.run(() => {
					 data.data = this.providerDoctors.transformDoctors(data.data);
					this.doctors = this.doctors.concat(data.data);
					this.loadDoctors = false;
					this.meta = data.meta;
					this.links = data.links;
				})
		})
	}
	
	
	
  //ISSUES
  loadI=false;
  metaI:any;
  linksI:any;
  loadIssues(id:number){
		this.issues = [];
		 this.zone.run(() => {
			this.loadI = true;
		});
	     this.providerIssues.get_issues_specialite(id).then( data=>{
			 this.zone.run(() => {
				this.issues = data.data;
				this.loadI = false;
				this.metaI = data.meta;
				this.linksI = data.links;
				console.log(this.issues );
			});
		})
  }
  
	moreI(){
		let id= this.providerSpacialite.specialty.id;
		 this.zone.run(() => {
			this.loadI = true;
		});
		 this.providerIssues.get_issues_specialite(id,this.linksI.next ).then( data=>{
			 this.zone.run(() => {
				this.issues = this.issues.concat(data.data);
				this.loadI = false;
				this.metaI = data.meta;
				this.linksI = data.links;
			});
		})
	}
	
	
  //PLACES
  loadC=true;
  metaC:any;
  linksC:any;
  loadPlaces(id:number){
		this.places = [];
		this.loadC = true;
	     this.providerPlaces.get_places_specialite(id).then( data=>{
				this.places = data.data;
				this.loadC = false;
				this.metaC = {
						last_page: data.last_page,
						from: data.from,
						per_page:data.per_page,
						to:data.to,
						total: data.total,
						current_page: data.current_page
					};
					this.linksC ={
							last:data.last_page_url,
							prev: data.prev_page_url,
						    next: data.next_page_url,
							first: data.first_page_url,
							
					};
				console.log(this.places );
		})
  }
  
	moreC(){
		let id= this.providerSpacialite.specialty.id;
		 this.zone.run(() => {
			this.loadC = true;
		});
		 this.providerPlaces.get_places_specialite(id,this.linksC.next ).then( data=>{
			 this.zone.run(() => {
				this.places = this.places.concat(data.data);
				this.loadC = false;
				this.metaC = {
						last_page: data.last_page,
						from: data.from,
						per_page:data.per_page,
						to:data.to,
						total: data.total,
						current_page: data.current_page
					};
					this.linksC ={
							last:data.last_page_url,
							prev: data.prev_page_url,
						    next: data.next_page_url,
							first: data.first_page_url,
							
					};
				console.log(this.places );
			});
		})
	}
	
	
	end($event){
		if (this.meta && this.meta.current_page < this.meta.last_page){
			let screen = this.contentDoctor.getContentDimensions();
			if (!this.loadDoctors && $event.directionX=="right" && ((screen.scrollWidth - (screen.contentWidth*1.5)) < screen.scrollLeft) ){
				this.moreDoctors();
			}
		}
	}
	
	endI($event){
		if (this.metaI && this.metaI.current_page < this.metaI.last_page){
			let screen = this.contentIssues.getContentDimensions();
			if (!this.loadI && $event.directionX=="right" && ((screen.scrollWidth - (screen.contentWidth*1.5)) < screen.scrollLeft) ){
				this.moreI();
			}
		}
	}
	
	endC($event){
		
		if (this.metaC && this.metaC.current_page < this.metaC.last_page){
			console.log("!!!!!!!!!!!!!!!!!!!!!!");
			let screen = this.contentPlaces.getContentDimensions();
			if (!this.loadC && $event.directionX=="right" && ((screen.scrollWidth - (screen.contentWidth*1.5)) < screen.scrollLeft) ){
				this.moreC();
			}
		}
	}
	
	
	scrollTo(elementId: string) {
		try{
			/* let y = document.getElementById(elementId);
			console.log(y);
			this.contentCategories.scrollTo(100, y);*/
			console.log(1234);
		}catch(e){
			console.log(e);
		}
    }
	
	 goToProfile(doctor:any, type: string){
		this.navCtrl.push('ProfilePage',{doctor:doctor, type: type});
	 }
	

}
