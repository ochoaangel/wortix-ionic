import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MyCitaPage } from './my-cita';

@NgModule({
  declarations: [
    MyCitaPage,
  ],
  imports: [
    IonicPageModule.forChild(MyCitaPage), TranslateModule
  ],
})
export class MyCitaPageModule {}
