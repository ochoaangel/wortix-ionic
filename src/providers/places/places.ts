import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppContants } from '../../app/app.constants'
import { UtilProvider} from '../util/util';
/*
  Generated class for the PlacesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlacesProvider {


  constructor(public http: HttpClient, public util: UtilProvider) {
    console.log('Hello PlacesProvider Provider');
  }
  
  transformPlace(place:any){
		 place.typeO = 'place';
		place.photo_url=   'https://img.freepik.com/free-vector/hospital-reception-with-flat-design_23-2147972689.jpg'
		return place;
	}
	
	transformPlaces(places:any){
		for (let i = 0; i < places.length; i++){
				places[i] = this.transformPlace(places[i]);
		}
		return places;
	}
  
    places:any;
	placesPromise:any;
	get_places_specialite(id:number, url?:string){
		if (url === undefined){
			 url =  AppContants.url_backend + "api/searchers/companies?specialty="+id+"&limit=2";
		}else{
			url = url+ "&specialty="+id+"&limit=2";
		}
		this.util.load(1);
		
		this.placesPromise=  this.http.get(url).toPromise()
		this.placesPromise.then(
		  (data:any) =>{
			   this.util.unload(1);
			this.places =data;
			for (let i = 0; i < data.length; i++){
				this.places[i].typeO = 'place';
				
			}
		}).catch(err => {
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba obtener las clinicas" ,"bottom",'Ok',2000)
		});
		return this.placesPromise;
	}
	
	get_places_search(keyword:string, url?:string){
		if (url === undefined){
			url = AppContants.url_backend + "api/searchers/companies?limit=10&name="+keyword+"&city="+keyword+"&address="+keyword;
		}else{
			url = url+ "&name="+keyword+"&city="+keyword+"&address="+keyword+"&limit=10";
		}
		 this.util.load(1);
		this.placesPromise=  this.http.get(url).toPromise()
		this.placesPromise.then(
		  (data:any) =>{
			this.util.unload(1);
			this.places =data;
			for (let i = 0; i < data.length; i++){
				this.places[i].typeO = 'place';
				
			}
		}).catch(err => {
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
		});
		return this.placesPromise;
	}

}
