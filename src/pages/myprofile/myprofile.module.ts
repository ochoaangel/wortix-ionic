import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MyprofilePage } from './myprofile';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MyprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(MyprofilePage),	TranslateModule, 
	ComponentsModule,
  ],
})
export class MyprofilePageModule {}
