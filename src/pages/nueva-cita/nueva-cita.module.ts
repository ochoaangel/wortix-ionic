import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NuevaCitaPage } from './nueva-cita';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NuevaCitaPage,
  ],
  imports: [
    IonicPageModule.forChild(NuevaCitaPage), TranslateModule,
	ComponentsModule
  ],
})
export class NuevaCitaPageModule {}
