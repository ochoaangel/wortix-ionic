import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the PaymentConfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-confirm',
  templateUrl: 'payment-confirm.html',
})
export class PaymentConfirmPage {

	doctor:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
	  this.doctor = this.navParams.get('doctor') || JSON.parse(localStorage.doctor) || {};
	localStorage.doctor = JSON.stringify(this.doctor);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentConfirmPage');
  }
  
  home(){
	  this.navCtrl.setRoot(HomePage, {});
  }

}
