import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { DoctorsProvider } from '../../providers/doctors/doctors';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
	stars = [1,2,3,4,5];
  doctor:any;
  verInfo:boolean=false
  constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public serviceAuth: AuthProvider,
				public serviceDoctor: DoctorsProvider) {
  }
  
  
  
  

  ionViewDidLoad() {
    this.doctor = this.navParams.get('doctor') || JSON.parse(localStorage.doctor) || {};
	localStorage.doctor = JSON.stringify(this.doctor);
	console.log(this.doctor);
	
	this.doctor.tipos = {};
	if ( this.doctor.types)
		for(let i = 0; i  < this.doctor.types.length; i++){
			this.doctor.tipos[this.doctor.types[i].id] = this.doctor.types[i]; 
			
			
		}
	
	
	this.serviceDoctor.get_like(this.doctor.id).then(
		(data:any)=> {
			console.log(data);
			this.doctor.likes =  data.likes;
		}
		
	);
  }
  
  ionViewDidEnter(){
	  this.serviceDoctor.MisFavoritos();
	  
  }
  
  reviewUser(tipo:number){
	  if (this.doctor.tipos[tipo]){
			this.serviceAuth.reviewUser().then( data => { 
				if (data == false){
					alert("Debe iniciar sesión para disfrutar del servicio")
				}else{
					this.navCtrl.push("NuevaCitaPage", {doctor: this.doctor, tipo:tipo});
				}
			}	
			);
	  }
  }
  
  
  goToMore(b:boolean){
	  // //alert(this.doctor.typeO);
	  // if (this.doctor.typeO == 'doctor')
		// this.navCtrl.push("InfouserPage", {doctor: this.doctor});
	
		this.verInfo = b;
  }
  
  favorite(b:boolean){
	  this.serviceDoctor.setFavoritos(this.doctor, b)
  }
  
  

}
