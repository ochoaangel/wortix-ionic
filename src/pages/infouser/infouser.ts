import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the InfouserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-infouser',
  templateUrl: 'infouser.html',
})
export class InfouserPage {
  doctor:any;
  
  
    options= [
			{ 	id: 1,
				name: "APPOINTMENTS",
				sub: " (Schedule / Pay For it)",
				ico_path: "/assets/imgs/search-p/1.png", 
				click: () => {
					this.navCtrl.push("AppoimentsPage");
				}
			},
			
			{ 	id: 2,
				name: "SPECIALTIES",
				sub: "(Cardiology, Gynecology, General Medicine, ….)",
				ico_path: "/assets/imgs/search-p/2.png",
				click: () => {
					this.navCtrl.push("SpecialitiesPage");
				}
			},

			{ 	id: 2,
				name: "HOUSE VISITS",
				sub: "(Doctors, Nurses, Therapists,….)",
				ico_path: "/assets/imgs/search-p/3.png",
				click: () => {
					this.navCtrl.push("MapaPage", {tipo: 1});
				}
			},
			{ 	id: 2,
				name: "TESTING CENTERS",
				sub: "(Radiology, Lab, Pharmacies,….)",
				ico_path: "/assets/imgs/search-p/4.png",
				click: () => {
					this.navCtrl.push("ListsPage", {tipo: 2});
				}
				
			},
			{ 	id: 2,
				name: "HEALTH ISSUES",
				sub: "(Anonymized cases)",
				ico_path: "/assets/imgs/search-p/5.png",
				click: () => {
					//this.navCtrl.push("ListsPage");
				}
			},
			
			
			
			
			
			
			
			
			
	]
	

  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.doctor = this.navParams.get('doctor') || JSON.parse(localStorage.doctor) || {};
	localStorage.doctor = JSON.stringify(this.doctor);
	console.log(this.doctor);
	this.doctor.tipos = {};
	for(let i = 0; i  < this.doctor.types.length; i++){
		this.doctor.tipos[this.doctor.types[i].id] = this.doctor.types[i]; 
	}
	console.log(this.doctor);
  }

}
