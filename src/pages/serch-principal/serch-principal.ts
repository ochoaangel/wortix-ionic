import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
/**
 * Generated class for the SerchPrincipalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-serch-principal',
  templateUrl: 'serch-principal.html',
})
export class SerchPrincipalPage {
   
    options= [
			
			{ 	id: 2,
				name: "HOUSE VISITS",
				sub: "(Doctors, Nurses, Therapists,….)",
				ico_path: "/assets/imgs/search-p/3.png",
				click: () => {
					this.navCtrl.push("MapaPage", {tipo: 'doctors'});
				}
			},
			
			
			
			{ 	id: 2,
				name: "SPECIALTIES",
				sub: "(Cardiology, Gynecology, General Medicine, ….)",
				ico_path: "/assets/imgs/search-p/2.png",
				click: () => {
					this.navCtrl.push("SpecialitiesPage");
				}
			},
			
			{ 	id: 1,
				name: "APPOINTMENTS",
				sub: " (Schedule / Pay For it)",
				ico_path: "/assets/imgs/search-p/1.png", 
				click: () => {
					this.navCtrl.push("AppoimentsPage");
				}
			},	

			{ 	id: 2,
				name: "TESTING CENTERS",
				sub: "(Radiology, Lab, Pharmacies,….)",
				ico_path: "/assets/imgs/search-p/4.png",
				click: () => {
					this.navCtrl.push("MapaPage", {tipo: 'testing-centers'});
				}
				
			},
			{ 	id: 2,
				name: "HEALTH ISSUES",
				sub: "(Anonymized cases)",
				ico_path: "/assets/imgs/search-p/5.png",
				click: () => {
					this.navCtrl.push("MyhealthcasePage", {tipo: 2});
				}
			},
			
			
			
			
			
			
			
			
			
	]
	

  categories= [
			{ 	id: 1,
				name: "cardioglogy",
				ico_path: "/assets/imgs/search-p/1.png"
			},
			{ 	id: 2,
				name: "radiology",
				ico_path: "/assets/imgs/category/2.png"
			},
			{ 	id: 3,
				name: "surgery",
				ico_path: "/assets/imgs/category/3.png"
			},
			{ 	id: 4,
				name: "neurology",
				ico_path: "/assets/imgs/category/4.png"
			},
			{ 	id: 5,
				name: "pediatrics",
				ico_path: "/assets/imgs/category/5.png"
			},
			
			{ 	id: 6,
				name: "ophtalmology",
				ico_path: "/assets/imgs/category/6.png"
			},
  ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SerchPrincipalPage', this);
	
  }
  
    goTo(path:string){
		this.navCtrl.push(path);
	 }
	 
  keyword:string="";
	getItems(){
		this.navCtrl.push("ListsPage", {tipo: 3, keyword: this.keyword});
	}

}
