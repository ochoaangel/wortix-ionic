import { Component, Input} from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilProvider} from '../../providers/util/util';

/**
 * Generated class for the WFooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'w-footer',
  templateUrl: 'w-footer.html'
})
export class WFooterComponent {
  constructor(public navCtrl: NavController, public authProvider: AuthProvider, public util: UtilProvider) {
   
  }

   goToHome(){
		this.navCtrl.setRoot(HomePage);
   }
   
   
   
   goToMensajes(){
		this.navCtrl.setRoot('BandejaEntradaPage');
   }
   
   
   reviewUser(){
	this.authProvider.reviewUser().then( data => { 
		console.log(data);
		if (data == false){
			alert("Debe iniciar sesión para disfrutar del servicio")
		}else{
			this.navCtrl.setRoot('MyprofilePage');
		}
	})	
  }
}
