import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoUploadPage } from './video-upload';

@NgModule({
  declarations: [
    VideoUploadPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoUploadPage), TranslateModule
  ],
})
export class VideoUploadPageModule {}
