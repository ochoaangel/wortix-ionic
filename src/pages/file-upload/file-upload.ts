import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FileUploadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-file-upload',
  templateUrl: 'file-upload.html',
})
export class FileUploadPage {
	
	issue:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
		this.issue = this.navParams.get('issue') || JSON.parse(localStorage.issue) || {};
	localStorage.issue = JSON.stringify(this.issue);
	console.log(this.issue);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FileUploadPage');
  }

}
