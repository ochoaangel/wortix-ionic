import { Component, NgZone,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { IssuesProvider } from '../../providers/issues/issues';
import { DoctorsProvider } from '../../providers/doctors/doctors';

/**
 * Generated class for the MyhealthcasePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-myhealthcase',
  templateUrl: 'myhealthcase.html',
})
export class MyhealthcasePage {

	 @ViewChild('contentIssues') contentIssues: Content
	 stars = [1,2,3,4,5];
  constructor(public navCtrl: NavController,
			  public navParams: NavParams,
			  private zone: NgZone,
			  public providerIssues: IssuesProvider,
			  public providerDoctor: DoctorsProvider
			  ) {
  }

  ionViewDidLoad() {
	  
	  this.loadIssues(12);
  }
  
  
  //ISSUES
  issues:any;
  loadI=false;
  metaI:any;
  linksI:any;
  from:any;
  keyword:string="";
  loadIssues(id:number){
	  
	   this.from = this.navParams.get('from') ||  localStorage.from || null;
	    localStorage.from = this.navParams.get('from');
		
		if (!this.from){
			this.from ='';
		}
		this.issues = [];
		 this.zone.run(() => {
			this.loadI = true;
		});
		if (this.from == 'profile'){
			this.providerIssues.get_my_issues_specialite().then( data=>{
				 this.zone.run(() => {
					this.issues = data.data;
					this.loadI = false;
					this.metaI = data.meta;
					this.linksI = data.links;
					console.log(this.issues );
				});
			})
		}else{
			this.providerIssues.get_issues_specialite(id).then( data=>{
				 this.zone.run(() => {
					this.issues = data.data;
					this.loadI = false;
					this.metaI = data.meta;
					this.linksI = data.links;
					console.log(this.issues );
				});
			})
			
		}
  }
  
	moreI(){
		let id= 12;
		 this.zone.run(() => {
			this.loadI = true;
		});
		if (this.from != 'profile'){
			 this.providerIssues.get_issues_specialite(id,this.keyword != ""?this.keyword:null, this.linksI.next ).then( data=>{
				 this.zone.run(() => {
					this.issues = this.issues.concat(data.data);
					this.loadI = false;
					this.metaI = data.meta;
					this.linksI = data.links;
				});
			})
		}else{
			 this.providerIssues.get_my_issues_specialite(this.keyword != ""?this.keyword:null, this.linksI.next ).then( data=>{
				 this.zone.run(() => {
					this.issues = this.issues.concat(data.data);
					this.loadI = false;
					this.metaI = data.meta;
					this.linksI = data.links;
				});
			})
			
		}
	}
	
	endI($event){
		if (this.metaI && this.metaI.current_page < this.metaI.last_page){
			let screen = this.contentIssues.getContentDimensions();
			if (!this.loadI && $event.directionX=="right" && ((screen.scrollWidth - (screen.contentWidth*1.5)) < screen.scrollLeft) ){
				this.moreI();
			}
		}
	}
	
	goToDoctor(doctor, type){
		if (type == 'doctor'){
			this.providerDoctor.get_doctor_one(46485/*doctor.id*/).then( doctor=>{
					console.log( doctor.data);
					this.navCtrl.push('ProfilePage',{doctor: doctor.data, type: type});
				}
			)
		}
		
	}
	
	goToMore(issue:any){
	  this.navCtrl.push("IssueInfoPage", {issue: issue});
	}
	
	
	buscar(event?:any, more?:boolean){
		//this.barra= false;
		
		if (this.keyword== ""){
			return;
		}
		if (!more){
			this.issues = [];
		}
		
		if (this.from == 'profile'){
			this.providerIssues.get_my_issues_specialite(this.keyword != ""?this.keyword:null).then( data=>{
				 this.zone.run(() => {
					this.issues = data.data;
					this.loadI = false;
					this.metaI = data.meta;
					this.linksI = data.links;
					console.log(this.issues );
				});
			})
		}else{
			this.providerIssues.get_issues_specialite(12, this.keyword).then( data=>{
				 this.zone.run(() => {
					this.issues = data.data;
					this.loadI = false;
					this.metaI = data.meta;
					this.linksI = data.links;
					console.log(this.issues );
				});
			})
			
		}
	}


}

