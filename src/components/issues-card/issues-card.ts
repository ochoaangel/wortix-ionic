import { Component, Input, NgZone } from '@angular/core';
import { IssuesProvider } from '../../providers/issues/issues';
import { DoctorsProvider } from '../../providers/doctors/doctors';
import {  NavController } from 'ionic-angular';

/**
 * Generated class for the IssuesCardComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'issues-card',
  templateUrl: 'issues-card.html'
})
export class IssuesCardComponent {

  @Input("issue") is: any;

  constructor( private zone: NgZone,
			  public providerIssues: IssuesProvider,
			  public providerDoctor: DoctorsProvider,
			  public navCtrl:NavController) {
				setTimeout(()=>  {
				  this.providerIssues.get_issue_cometarios(this.is);
				}, 300)
	
  }
  
  goToMore(issue:any){
    this.navCtrl.push("IssueInfoPage", {issue: issue});
    
   }

}
