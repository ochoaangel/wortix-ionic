import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { IssueInfoPage } from './issue-info';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    IssueInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(IssueInfoPage), TranslateModule,
	ComponentsModule
  ],
})
export class IssueInfoPageModule {}
