import { Component, ViewChild , NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, Content } from 'ionic-angular';
import { LocationProvider } from '../../providers/location/location';
import { UtilProvider} from '..//../providers/util/util';
import { AppContants } from '../../app/app.constants';
import { SpecialiteProvider } from '../../providers/specialite/specialite'
import { DoctorsProvider } from '../../providers/doctors/doctors';


/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {
	@ViewChild('contentDoctor') contentDoctor: Content;
  users:any=[];       // LISTADO DE DOCTORES
  filtro:string="doctors";
  type:any=null;
  doctorSel:any=null;
  categorySel:any;
 //  public service = new google.maps.places.AutocompleteService();
searchF:boolean=false;

 
 categories:any;
	categories2:any=[];
	
  constructor(public navCtrl: NavController,   public providerDoctor: DoctorsProvider, public navParams: NavParams, private zone: NgZone,  public util: UtilProvider,  public serviceLocation: LocationProvider,public actionSheetCtrl: ActionSheetController,  public providerSpacialite: SpecialiteProvider) {
  }

	
	ionViewDidLoad() {
		this.filtro = this.navParams.get('tipo') || localStorage.filtro || false;
		
		if (!this.filtro){
			this.filtro = "doctors";
		}
		localStorage.filtro = this.filtro;
		this.serviceLocation.typeActual = this.filtro;
		this.serviceLocation.callbackGD = (data:any) => { 
						console.log(data)
						this.users =data.data[this.filtro];
						this.util.unload(2);
						console.log(this.users);
						for (let i = 0; i < this.users.length; i++){
							let us = this.users[i];
							this.users[i].selfClick= (tipo)	=> {
								this.zone.run(() => {
									this.scrollTo("item-"+us.id);
								//	if (this.doctorSel != us){
										this.doctorSel = us;
										//doctor.moveTome()
									//}else{
										
										this.goToDoctor(us,  'doctor', tipo);
										
									//}
								});
							}
						}
		};
		this.serviceLocation.iniciarMapa();
		this.providerSpacialite.get_all().then( data => {
				this.categories = this.providerSpacialite.specialties;
				let j = 0;
				for(let i =0; i< this.categories.length; i++){
					if ( i % 2 == 0){
						this.categories2[j]=[];
						this.categories2[j].push(this.categories[i]);
						this.categories2[j].push({id: false});
						
					}else{
						this.categories2[j][1]= this.categories[i];
						j++;
					}
				}
				console.log(this.categories2);
			}
		);
		this.util.showToast("El pin azul indica el lugar a donde el medico ira para la consulta. Cololoquelo en el punto que desea" ,"bottom",'Ok',2000)
	}

	ionViewWillEnter(){
		//this.serviceLocation.get_doctor_by_distance(36000, -12.172781 , -76.934686)
		this.serviceLocation.noSearch = false;
		this.serviceLocation.moveToUserPin = true;
		this.serviceLocation.cameraMoveOn = true;
		this.serviceLocation.haveCircle = false;
		this.serviceLocation.clearMarkers();
		if(this.serviceLocation.map!=null){
			this.serviceLocation.map.setDiv('map_canvas');
			this.serviceLocation.map.setVisible(true);
		    setTimeout(()=>{ 
			   try{
				   this.serviceLocation.map.setDiv('map_canvas');
				   this.serviceLocation.map.setVisible(true);
				  
			   }catch(e){
				   alert(e);
			   }
		    },1000);
		}
		
		
	}
	
	
	ionViewWillLeave(){
		 this.serviceLocation.map.setDiv(null);
		
	}
	
	presentActionSheet(doctor:any, type: string) {
		const actionSheet = this.actionSheetCtrl.create({
		  
		  buttons: [
			{
			  text: 'Follow',
			  role: 'Privacy',
			  cssClass: 'btn-action',
			  handler: () => {
				
			  }
			}, {
			  text: 'Contact',
			  cssClass: 'btn-action',
			  handler: () => {
				
			  }
			}, {
			  text: 'Profile',
			  cssClass: 'btn-action',
			  handler: () => {
				this.goToDoctor(doctor, type, 2)
				
			  }},
			  
			 {
			  text: 'Report',
			  cssClass: 'btn-action danger',
			  handler: () => {
				console.log('Cancel clicked');
				
			  }
			}
		  ]
		});
		actionSheet.present();
	}
	
	searching:boolean=false;
	keyword:string="";
	km:number=0;
	tmSearch:any= null;
	tmSearch2:any= null;
	buscar($event?, onlyRange?){
		if (!this.searching){
			clearTimeout(this.tmSearch);
			this.tmSearch = setTimeout(()=>{
					this.searching = true;
					if (!onlyRange){
						let bsc = this.keyword;
						this.serviceLocation.buscarEnMapa(this.keyword).then(() => {
							this.searching = false;
							if (bsc != this.keyword){
								this.buscar();
							}
						})
					}else{
						
						this.serviceLocation.research().then(() => {
							this.searching = false;
							
						})
					}
			},1000);
		}else{
			if (!this.tmSearch2){
				this.tmSearch2 = setTimeout(()=>{
					this.searching = false 
					console.log('reset')
					clearTimeout(this.tmSearch2);
				},5000);
			}
			
		}
	}
	
	selectPlace(place){
		
		 this.keyword = "";
		 for(let i = 0; i < place.extra.lines.length;i++){
			 
			  this.keyword += place.extra.lines[i] + (i < place.extra.lines.length-1?', ':'');
		 }
		 
		 let datos={position:place.position,sitio: this.keyword };
			   		 this.serviceLocation.procesarBusqueda(datos);
		 
	}
	
	s(){
		 setTimeout(()=>{
			 
			 this.searchF=false;
		 },1000)
		
	}
	
	
	goToProfile(doctor:any, type: string){
		this.serviceLocation.noRefresh=true;
		doctor.moveTome()
	}
	
	scrollTo(elementId: string) {
		try{
			let y = document.getElementById(elementId).offsetTop;
			this.contentDoctor.scrollTo(100, y);
		}catch(e){
			console.log(e);
		}
    }
	
	open(){
		this.type=this.type == null?  'doctor': null
		if (this.type !=null && this.doctorSel){
			setTimeout(()=>{
					this.scrollTo("item-"+this.doctorSel.id);
				},300);
		}
	}
	
	
	goToDoctor(doctor, type, by){
		if (this.filtro == 'doctors'){
			this.providerDoctor.get_doctor_one(doctor.id).then( doctor=>{
					if (by==2)
						this.navCtrl.push('ProfilePage',{doctor: doctor.data, type: type});
					else
						this.navCtrl.push("NuevaCitaPage", {doctor: doctor.data, tipo:2});
				}
			)
		}else{
				if (by==2)
						this.navCtrl.push('ProfilePage',{doctor: doctor, type: this.filtro});
					else
						this.navCtrl.push("NuevaCitaPage", {doctor: doctor, tipo:this.filtro=='professional'?2:1});
			
		}
		
	}
	
	tipoChange(){
		this.serviceLocation.typeActual = this.filtro;
		this.serviceLocation.specialty = this.categorySel && this.categorySel != "null"? this.categorySel : null;
		this.buscar(null, true);
		
	}
	
}
