import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentConfirmPage } from './payment-confirm';

@NgModule({
  declarations: [
    PaymentConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentConfirmPage), TranslateModule,
  ],
})
export class PaymentConfirmPageModule {}
