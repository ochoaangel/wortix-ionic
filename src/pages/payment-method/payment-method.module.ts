import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentMethodPage } from './payment-method';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PaymentMethodPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentMethodPage), TranslateModule,
	ComponentsModule
  ],
})
export class PaymentMethodPageModule {}
