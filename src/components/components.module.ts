import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FooterComponent } from './footer/footer';
import { WFooterComponent } from './w-footer/w-footer';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from 'ionic-angular';

import {
 /* MatAutocompleteModule,
  //MatBadgeModule,
  //MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,*/
  MatProgressBarModule
/*  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  //MatTreeModule,
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher*/
} from '@angular/material';
import { IssuesCardComponent } from './issues-card/issues-card';




@NgModule({
	declarations: [FooterComponent,
    WFooterComponent,
    IssuesCardComponent],
	imports: [IonicModule, MatProgressBarModule, TranslateModule],
	exports: [FooterComponent,
    WFooterComponent,
    IssuesCardComponent],
	
})
export class ComponentsModule {}
