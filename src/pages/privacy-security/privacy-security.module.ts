import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PrivacySecurityPage } from './privacy-security';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PrivacySecurityPage,
  ],
  imports: [
    IonicPageModule.forChild(PrivacySecurityPage),TranslateModule,
	ComponentsModule
  ],
})
export class PrivacySecurityPageModule {}
