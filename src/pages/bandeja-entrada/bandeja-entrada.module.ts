import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { BandejaEntradaPage } from './bandeja-entrada';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    BandejaEntradaPage,
  ],
  imports: [
    IonicPageModule.forChild(BandejaEntradaPage), TranslateModule,
	ComponentsModule
  ],
})
export class BandejaEntradaPageModule {}
