import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoCallPage } from './video-call';

@NgModule({
  declarations: [
    VideoCallPage,
  ],
  imports: [
    IonicPageModule.forChild(VideoCallPage), TranslateModule
  ],
})
export class VideoCallPageModule {}
