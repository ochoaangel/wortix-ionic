import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ClinicHistoryPage } from './clinic-history';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    ClinicHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ClinicHistoryPage), TranslateModule,
	ComponentsModule
  ],
})
export class ClinicHistoryPageModule {}
