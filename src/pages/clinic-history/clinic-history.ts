import { Component, NgZone} from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { IssuesProvider } from '../../providers/issues/issues';


/**
 * Generated class for the ClinicHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-clinic-history',
  templateUrl: 'clinic-history.html',
})
export class ClinicHistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,  public authService:AuthProvider,  public providerIssues: IssuesProvider,  private zone: NgZone, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    this.providerIssues.get_my_clinic_history().then( data=>{
				// this.zone.run(() => {
					
					console.log(data );
			//	});
			})
  }
  
  active(disease){
	  if(!this.providerIssues.myDiseasesMap[disease.id]){
		  this.providerIssues.myDiseasesMap[disease.id] =  disease;
		  this.providerIssues.myDiseasesMap[disease.id].active =  true;
	  }else{
			this.providerIssues.myDiseasesMap[disease.id].active =  !this.providerIssues.myDiseasesMap[disease.id].active;
	  }
	  
	  this.providerIssues.diseasesUpdate();
  }
  
  addAlergia(){
	    let alergiaModal = this.modalCtrl.create('AllergiesPage', {  });
		alergiaModal.onDidDismiss(data => {
			 this.providerIssues.get_my_clinic_history().then( data=>{
				 this.zone.run(() => {
					console.log(data );
				 });
			})
		});
		alergiaModal.present();
	  
  }
  
  
  addAntecendentes(){
	    let alergiaModal = this.modalCtrl.create('AntecedantsPage', {  });
		alergiaModal.onDidDismiss(data => {
			 this.providerIssues.get_my_clinic_history().then( data=>{
				 this.zone.run(() => {
					console.log(data );
				 });
			})
		});
		alergiaModal.present();
	  
  }

}
