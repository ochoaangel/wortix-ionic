import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SpecialiteProvider } from '../../providers/specialite/specialite'

/**
 * Generated class for the SpecialitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-specialities',
  templateUrl: 'specialities.html',
})
export class SpecialitiesPage {
	categories:any;
	categories2:any=[];
	
	goTo(path:string){
		console.log(path);
		this.navCtrl.push(path);
	 }
	 

  constructor(public navCtrl: NavController, public navParams: NavParams, public providerSpacialite: SpecialiteProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialitiesPage');
	this.providerSpacialite.get_all().then( data => {
			this.categories = this.providerSpacialite.specialties;
			let j = 0;
			for(let i =0; i< this.categories.length; i++){
				if ( i % 2 == 0){
					this.categories2[j]=[];
					this.categories2[j].push(this.categories[i]);
					this.categories2[j].push({id: false});
					
				}else{
					this.categories2[j][1]= this.categories[i];
					j++;
				}
			}
			console.log(this.categories2);
		}
		);
	}

	selectSpecialitie(spacialite:any){
		this.providerSpacialite.specialty =spacialite;
			this.navCtrl.push("SpeciaPage", {'sp': spacialite});
		
	}
	
	

}
