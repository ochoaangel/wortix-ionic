import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppContants } from '../../app/app.constants'
import { UtilProvider} from '../util/util';

/*
  Generated class for the SpecialiteProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SpecialiteProvider {

	specialties:any=[];
	specialty:any;
	ico = {12:   "wortixgeneral-medicine"             ,
				1:   "wortixalergics1"           ,
				3:    "wortixcardiology"            ,
				6:     "wortixdermatology"           ,
				9:     "wortixendocrinology"           ,
				11:     "wortixgastroenterologist"           ,
				15:      "wortixgeriatrics"          ,
				17:      "wortixhiv"          ,
				20:      "wortixinmunology"          ,
				21:      "wortixinternal-medicine"          ,
				41:       "wortixlaboratory"         ,
				24:        "wortixnefrology"        ,
				25:      "wortixneumology"          ,
				26:       "wortixneurology"          ,
				27:    "wortixneurology-surgery"              ,
				28:    "wortixginecology"              ,
				29:    "wortixodontology"              ,
				2:     "wortixgeneral-medicine"             ,
				30:    "wortixgeneral-medicine"              ,
				31:    "wortixgeneral-medicine"              ,
				40:    "wortixpharmacy"              ,
				33:    "wortixgeneral-medicine"              ,
				34:    "wortixgeneral-medicine"              ,
				35:    "wortixgeneral-medicine"              ,
				36:    "wortixpsiquiatry"              ,
				43:    "wortixpsicology"              ,
				42:    "wortixgeneral-medicine"              ,
				37:    "wortixgeneral-medicine"              ,
				44:    "wortixsexology"              ,
				38:    "wortixtraumatology"              ,
				39:    "wortixurology"              }
	constructor(public http: HttpClient, public util: UtilProvider) {
		console.log('Hello SpecialiteProvider Provider');
	}

  
    specialtiesPromise:any;
	get_all(){
		console.log(this.specialtiesPromise);
		if (this.specialtiesPromise == null){
			this.util.load(1);
		
			this.specialtiesPromise=  this.http.get( AppContants.url_backend + "api/companies/222/specialties").toPromise()
			this.specialtiesPromise.then(
			  (data:any) =>{
				this.util.unload(1);
				let str = "["
				for (let i = 0; i < data.specialties.length; i++){
					 data.specialties[i].ico = this.ico[data.specialties[i].id]
				}
				this.specialties =data.specialties;
				this.specialty =this.specialties[0];
				
				
			}).catch(err => {
				console.error(err);
				this.util.unload(1);
			});
		}
		return this.specialtiesPromise;
	}
}
