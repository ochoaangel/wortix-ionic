import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { MisCitasPage } from './mis-citas';

@NgModule({
  declarations: [
    MisCitasPage,
  ],
  imports: [
    IonicPageModule.forChild(MisCitasPage), TranslateModule
  ],
})
export class MisCitasPageModule {}
