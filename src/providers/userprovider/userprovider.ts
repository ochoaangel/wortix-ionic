import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppContants } from '../../app/app.constants';
import { UtilProvider} from '../util/util';
import { AuthProvider} from '../auth/auth';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';

/*
  Generated class for the UserproviderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserproviderProvider {
	

  constructor(private afs: AngularFirestore, public http: HttpClient,  public util: UtilProvider) {
    console.log('Hello UserproviderProvider Provider');
  }

	generateUserObjSaveFromGoogle(user_data:any){
		return {
			name: user_data.additionalUserInfo.profile.given_name,
			last_name: user_data.additionalUserInfo.profile.family_name,
			email: user_data.additionalUserInfo.profile.email,
			password: '12312312',
			display_name:user_data.additionalUserInfo.profile.name
		}
	}
	
	generateUserObjSaveFromEmail(user_data:any){
		return {
			name: user_data.name,
			last_name: user_data.last_name,
			email: user_data.email,
			password: user_data.password,
			display_name:user_data.name
		}
	}
	
	/*
		Crea un nuevo usuario en la base de datos de GCP
		user_data: objeto con los datos del usuario;
		type: tipo de login
			0: Email And Password,
			1: Google
	*/

	createUser(user_data:any, type:number){
		if (type == 0){
			user_data = this.generateUserObjSaveFromEmail(user_data);
		}else if ( type == 1){
			user_data = this.generateUserObjSaveFromGoogle(user_data);
		}
		console.log(user_data);
		return new Promise( (resolve, rejected) => {
			this.http.post( AppContants.url_backend + "api/register/patient", user_data).toPromise().then(
			  (data:any) =>{
				console.log(data);
				resolve(data);
			}).catch(err => {
				console.error(err);
				rejected(err);
			});
			
		})
	}
	
	
	
	/*
		Crea un nuevo usuario de tipo doctor en la base de datos de GCP
		user_data: objeto con los datos del usuario;
		type: tipo de login
			0: Email And Password,
			1: Google
	*/

	createUserDoctor(user_data:any, type:number){
		if (type == 0){
			user_data = this.generateUserObjSaveFromEmail(user_data);
		}else if ( type == 1){
			user_data = this.generateUserObjSaveFromGoogle(user_data);
		}
		return new Promise( (resolve, rejected) => {
			this.http.post( AppContants.url_backend + "api/register/doctor", user_data).toPromise().then(
			  (data:any) =>{
				console.log(data);
				resolve(data);
			}).catch(err => {
				console.error(err);
				rejected(err);
			});
			
		})
	}


	
	
}
