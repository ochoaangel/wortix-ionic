import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentPage } from './payment';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    PaymentPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentPage), TranslateModule,
		ComponentsModule,
  ],
})
export class PaymentPageModule {}
