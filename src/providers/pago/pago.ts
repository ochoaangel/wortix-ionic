import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
//import { Stripe } from '@ionic-native/stripe';
import {  Platform } from 'ionic-angular';
import { UtilProvider} from '../util/util';

/*
  Generated class for the PagoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PagoProvider {

	pagoPaypal:any;
  constructor(public http: HttpClient,
				private payPal: PayPal,
				//private stripe: Stripe, 
				public platform: Platform,
				public util: UtilProvider) {
	    this.platform.ready().then(() => {
			this.payPal.init({
			  PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
			  PayPalEnvironmentSandbox: 'AVfA04OoFk2QaUPrRHUXrxtKu0MY3A3rYu2pTRWm7Gu_TDn75oLIxdmharPFGTbCKwIp-SWp4c-SfeNj'
			}).then(() => {
				// Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
				this.pagoPaypal = this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
				// Only needed if you get an "Internal Service Error" after PayPal login!
				//payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
				}) 
				)
			})
			
			//this.stripe.setPublishableKey('pk_test_mw6UCYcAU6LdjBwtOLrPD6xw');
		})
  }


	public pagarPaypal(valor: number, description: string){
		return new Promise((resolve,rejected)=>{
			try{
				this.pagoPaypal.then(() => {
					let payment = new PayPalPayment(valor + "", 'USD', description, 'sale');
					this.payPal.renderSinglePaymentUI(payment).then(() => {
						resolve(true)
					}, () => {
						resolve(false)
					});
				  }, () => {
					alert(12345);
				  });
			}catch(e){
				resolve(true)
				
			}
		});
		
	
	}
	
	
	public pagarStripe(){
		
		
	}
}