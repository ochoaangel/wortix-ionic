import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationConfigPage } from './notification-config';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NotificationConfigPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificationConfigPage), TranslateModule,
	ComponentsModule
  ],
})
export class NotificationConfigPageModule {}
