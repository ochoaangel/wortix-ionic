/*
	INTERFAZ PARA LOS USUARIOS LOGUEADOS EN LA APP
*/

export interface User	 {
    id?: string;
	name?:string;
	last_name?:string;
	email?:string;
	token?:string;
}
