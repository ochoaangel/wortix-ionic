import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoriteDoctorsPage } from './favorite-doctors';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FavoriteDoctorsPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoriteDoctorsPage), TranslateModule,
		ComponentsModule,
  ],
})
export class FavoriteDoctorsPageModule {}
