import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DoctorsProvider } from '../../providers/doctors/doctors';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the BandejaEntradaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bandeja-entrada',
  templateUrl: 'bandeja-entrada.html',
})
export class BandejaEntradaPage {
	role:string='doctor';
	role2:string='patient';
  constructor(public navCtrl: NavController, public navParams: NavParams, public doctorProvider:DoctorsProvider, public auth:AuthProvider ) {
  }

  ionViewDidLoad() {
    this.role = this.auth.userL.user.role;
    this.role2 =  this.role=='doctor'?'patient':'doctor';
	this.doctorProvider.getMyChatMessages();
	
  }

	goToMessages(room){
		this.navCtrl.push("ChatUserPage", {room: room});
		
	}
}
