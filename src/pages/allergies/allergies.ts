import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { IssuesProvider } from '../../providers/issues/issues';

/**
 * Generated class for the AllergiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-allergies',
  templateUrl: 'allergies.html',
})
	export class AllergiesPage {

	  nalergia:any={};
	  constructor(public navCtrl: NavController, public navParams: NavParams, public issuesProvider:IssuesProvider ) {
	  }

	  ionViewDidLoad() {
		console.log('ionViewDidLoad AllergiesPage');
	  }
	  
	  
	  nueva_alergia(){
		  this.issuesProvider.nueva_alergia(this.nalergia);
		  this.navCtrl.pop();
	  }

	}
