import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocationProvider } from '../../providers/location/location';
import { AuthProvider } from '../../providers/auth/auth';


/**
 * Generated class for the MapaUbicationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mapa-ubication',
  templateUrl: 'mapa-ubication.html',
})
export class MapaUbicationPage {

	kmA:number = 36;
	searchF:boolean=false;
	procesando:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,  public serviceLocation: LocationProvider, public authProvider: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapaUbicationPage');
	this.serviceLocation.iniciarMapa();
  }

	ionViewWillEnter(){
		this.serviceLocation.noSearch = true;
		this.serviceLocation.moveToUserPin = true;
		this.serviceLocation.cameraMoveOn = false;
		this.serviceLocation.haveCircle = true;
		
		if(this.serviceLocation.map!=null){
			this.serviceLocation.map.setDiv('map_canvas');
			this.serviceLocation.map.setVisible(true);
		    setTimeout(()=>{ 
			   try{
				  this.serviceLocation.putCircle()
				   this.serviceLocation.map.setDiv('map_canvas');
				   this.serviceLocation.map.setVisible(true);
				  
			   }catch(e){
				   alert(e);
			   }
		    },1000);
		}
		
		
	}
	
	searching:boolean=false;
	keyword:string="";
	km:number=0;
	tmSearch:any= null;
	tmSearch2:any= null;
	buscar($event?, onlyRange?){
		if (!this.searching){
			clearTimeout(this.tmSearch);
			this.tmSearch = setTimeout(()=>{
					this.searching = true;
					if (!onlyRange){
						let bsc = this.keyword;
						this.serviceLocation.buscarEnMapa(this.keyword).then(() => {
							this.searching = false;
							if (bsc != this.keyword){
								this.buscar();
							}
						})
					}else{
						
						this.serviceLocation.research().then(() => {
							this.searching = false;
							
						})
					}
			},1000);
		}else{
			if (!this.tmSearch2){
				this.tmSearch2 = setTimeout(()=>{
					this.searching = false 
					console.log('reset')
					clearTimeout(this.tmSearch2);
				},5000);
			}
			
		}
	}
	
	ionViewWillLeave(){
		 this.serviceLocation.map.setDiv(null);
		
	}
	
	selectPlace(place){
		
		 this.keyword = "";
		 for(let i = 0; i < place.extra.lines.length;i++){
			 
			  this.keyword += place.extra.lines[i] + (i < place.extra.lines.length-1?', ':'');
		 }
		 
		 let datos={position:place.position,sitio: this.keyword };
			   		 this.serviceLocation.procesarBusqueda(datos);
		 
	}
	
	s(){
		 setTimeout(()=>{
			 
			 this.searchF=false;
		 },1000)
		
	}
	
	
	
	thatIsMyplace(){
		let pos:any = this.serviceLocation.datosMapa.mk.getPosition();
		this.procesando= true;
		this.authProvider.saveMeLatLong( pos.lat, pos.lng, this.kmA/4*1604.34 ).then( 
			(data)=>{
				this.procesando= false;
				
			}
		);
	}
	
}
