import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { AcountPage } from './acount';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AcountPage,
  ],
  imports: [
    IonicPageModule.forChild(AcountPage),
	TranslateModule,
	ComponentsModule
  ],
})
export class AcountPageModule {}
