import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

	 options= [
			{ 	id: 1,
				name: "Notification",
				ico: "ios-notifications-outline", 
				click: () => {
					this.navCtrl.push("NotificationConfigPage");
				}
			},
			{ 	id: 1,
				name: "Privacy and Security",
				ico: "ios-lock-outline", 
				click: () => {
					let options = 'location=no,closebuttoncaption=X';
					if(this.platform.is('android')){options = 'location=no,hardwareback=no,footer=yes,closebuttoncaption=X,footercolor=#1B1B1B,closebuttoncolor=#488aff'}
					window.open('https://firebase.google.com/support/privacy', '_blank', options);//, '_system');
				}
			},
		/*	{ 	id: 1,
				name: "Payments",
				ico: "ios-card", 
				click: () => {
					this.navCtrl.push("PaymentMethodPage");
				}
			},*/
			
			
			{ 	id: 1,
				name: "Account",
				ico: "ios-person-outline", 
				click: () => {
					this.navCtrl.push("AcountPage", {type:  "editAccount"});
				}
			},
			
			
			
			{ 	id: 1,
				name: "About",
				ico: "ios-information-circle-outline", 
				click: () => {
					this.navCtrl.push("AboutPage");
				}
			},
			
			{ 	id: 2,
				name: "Sign Out ",
				
				ico: "log-out",
				click: () => {
				
					this.cerrar_session();
				}
				
			},
			
			
			
			
	]
  constructor(public navCtrl: NavController, public navParams: NavParams,  private platform: Platform, public authService:AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }

	cerrar_session(){
	  this.authService.signOut();
	  this.navCtrl.pop();
	  
  }
}
