import { HttpClient } from '@angular/common/http';
import { Injectable , NgZone} from '@angular/core';
import { AppContants } from '../../app/app.constants'
import { UtilProvider} from '../util/util';
import { AuthProvider} from '../auth/auth';
import { map } from 'rxjs/operators';
/*
  Generated class for the IssuesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IssuesProvider {

  constructor(public http: HttpClient, public util: UtilProvider, public auth: AuthProvider, private zone: NgZone) {
    console.log('Hello IssuesProvider Provider');
  }

	issues:any;
	issuesPromise:any;
	diseases:any;
	allergies:any;
	antecedents:any;
	myDiseases:any;
	myDiseasesMap:any=[];
	
	get_issues_specialite(id:number, word?:string, url?:string){
		//let url="";
		if (url === undefined){
			if(!word)
				url =  AppContants.url_backend + "api/health-issues?company=222&specialty="+id+"&limit=5";
			else
				url =  AppContants.url_backend + "api/searchers/health-issues?title="+word;/*+"&doctor="+word+"&patient="+word;*/
		}else{
			url = url+ "&specialty="+id+"&limit=5";
			if (word){
				url += url +"&title="+word;//+"&doctor="+word+"&patient="+word;
			}
		}
		this.util.load(1);
		
		this.issuesPromise=  this.http.get(url).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			  this.util.unload(1);
		
			this.issues =data;
		}).catch(err => {
			console.error(err);
				 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
		
		
	}
	
	get_my_issues_specialite(word?:string, link?:string){
		//alert("pass");
		let url =  AppContants.url_backend + "/api/patients/issues?api_token="+this.auth.userL.user.api_token;
		 url =  word?AppContants.url_backend + "api/patients/issues?api_token="+this.auth.userL.user.api_token+"&title="+word:url;
		if (link) {
			url  = link +"&api_token="+this.auth.userL.user.api_token;
			if (word){
				url += url +"&title="+word;//+"&doctor="+word+"&patient="+word;
			}
			
		}else if (this.auth.userL.user.role == 'doctor'){ 
			url =	  AppContants.url_backend +  '/api/doctors/health-issues?doctor='+this.auth.userL.user.id + "&api_token="+this.auth.userL.user.api_token;
		}
		
		this.util.load(1);
		this.issuesPromise=  this.http.get(url).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			this.issues =data;
		}).catch(err => {
			//alert("error");
			this.util.unload(1);
			this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
	}
	
	get_issue_cometarios(is:any){
		//alert("pass");
		let url =  AppContants.url_backend + "api/health-issues/"+is.id+"/comments?api_token="+this.auth.userL.user.api_token;	
		//this.util.load(1);
		this.issuesPromise=  this.http.get(url).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			is.comentarios=data.comments;
			if (is.comentarios.length > 0){
				is.last_comentario = is.comentarios [is.comentarios.length - 1 ]
				console.log(is);
			}
		}).catch(err => {
			//alert("error");
			//this.util.unload(1);
			//this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
	}
	
	
	
	get_my_clinic_history(){
		//alert("pass");
		let url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/medical-history/health-issues?api_token="+this.auth.userL.user.api_token;
		this.util.load(1);
		this.issuesPromise= this.http.get(url).toPromise().then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			this.issues =data;
		}).catch(err => {
			//alert("error");
			console.error(err);
			this.util.unload(1);
			this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
		});
		
		url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/medical-history/files?api_token="+this.auth.userL.user.api_token;
		this.util.load(1);
		this.issuesPromise= this.http.get(url).toPromise().then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			this.issues =data;
		}).catch(err => {
				//alert("error");
			console.error(err);
			this.util.unload(1);
			this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los archivos" ,"bottom",'Ok',2000)
		});
		
		url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/medical-history/body?api_token="+this.auth.userL.user.api_token;
		this.util.load(1);
		this.issuesPromise= this.http.get(url).toPromise().then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			this.issues =data;
		}).catch(err => {
				//alert("error");
			console.error(err);
			this.util.unload(1);
			this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
		});
		
		url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/medical-history/antecedents?api_token="+this.auth.userL.user.api_token;
		this.util.load(1);
		this.issuesPromise= this.http.get(url).toPromise().then(
		  (data:any) =>{
			    this.zone.run(() => {
					//alert("pass");
				  this.util.unload(1);
			
				this.antecedents =data.data;
				})
		}).catch(err => {
				//alert("error");
			console.error(err);
		});
		
		url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/medical-history/allergies?api_token="+this.auth.userL.user.api_token;
		this.util.load(1);
		this.issuesPromise= this.http.get(url).toPromise().then(
		  (data:any) =>{
			   this.zone.run(() => {
					//alert("pass");
					this.util.unload(1);
			
					this.allergies =data.data;
			   })
		}).catch(err => {
				//alert("error");
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba obtener las alergias" ,"bottom",'Ok',2000)
		});
		
		url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/diseases?api_token="+this.auth.userL.user.api_token;
		this.util.load(1);
		this.issuesPromise= this.http.get(url).toPromise().then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			this.myDiseases =data.data;
			this.myDiseasesMap=[];
			for (let i = 0; i < this.myDiseases.length; i++ ){
				this.myDiseases[i].active = true;
				this.myDiseasesMap[this.myDiseases[i].id] = this.myDiseases[i];
			}
			
		}).catch(err => {
				//alert("error");
				 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
			console.error(err);
		});
		
		
		url =  AppContants.url_backend + "api/diseases?api_token="+this.auth.userL.user.api_token;
		this.util.load(1);
		this.issuesPromise= this.http.get(url).toPromise().then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
				this.diseases = data.data;
				console.log(this.diseases);

			this.issues =data;
		}).catch(err => {
				//alert("error");
				 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba obtener los casos de salud" ,"bottom",'Ok',2000)
			console.error(err);
		});
		
		
		
		
		return this.issuesPromise;
	}
	
	
	
	nueva_cita(data:any, files:Array<File>){
		//alert("pass");
		let url =  AppContants.url_backend + "api/patients/issues";
		let formData = new FormData();
		let keys = Object.keys(data);
		for (let i =0; i < keys.length; i++){
			//if (keys[i] != 'specialty_id')
				formData.append(keys[i], data[keys[i]]);
		}
		this.util.load(1);
		if (files.length > 0){
			for (let i =0; i < files.length; i++){
				formData.append('files[]', files[i], files[i].name);
			}
		}
		formData.append('api_token', this.auth.userL.user.api_token);
		data.api_token= this.auth.userL.user.api_token;
		this.issuesPromise=  this.http.post(url, formData).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			//this.issues =data;
		}).catch(err => {
			//	//alert("error");
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba agendar una nueva cita" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
	}
	
	comentar(id:any, texto:string, file: File){
		let url =  AppContants.url_backend + "api/health-issues/"+id+"/comments";
		this.util.load(1);
		let formData = new FormData();
		if (file)
			formData.append('file', file, file.name);
		formData.append('api_token', this.auth.userL.user.api_token);
		formData.append('comment', texto);
		;
		this.issuesPromise=  this.http.post(url, formData).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
				this.util.showToast("Comentario realizado" ,"bottom",'Ok',2000);
				
			//this.issues =data;
		}).catch(err => {
				//alert("error");
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba agendar una nueva cita" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
		
	}
	
	move(id:any, estado:string){
		let url =  AppContants.url_backend + "api/pipeline/health-issues/"+id+"/move";
		this.util.load(1);
		let data={ api_token: this.auth.userL.user.api_token, status: estado} ;
		this.issuesPromise=  this.http.post(url, data).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
				this.util.showToast("Estado Actualizado" ,"bottom",'Ok',2000);
				
			//this.issues =data;
		}).catch(err => {
				//alert("error");
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba agendar una nueva cita" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
		
	}
	nueva_alergia(data:any){
		let url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/medical-history/allergies";
		this.util.load(1);
		data.api_token= this.auth.userL.user.api_token;
		let alergiaPromise=  this.http.post(url, data).toPromise()
		alergiaPromise.then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
			   this.get_my_clinic_history().then( data=>{
				 this.zone.run(() => {
					console.log(data );
				 });
				})
			  
		
			//this.issues =data;
		}).catch(err => {
				//alert("error");
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba guardar la alergia" ,"bottom",'Ok',2000)
		});
		return alergiaPromise;
	}
	
	nueva_antecendente(data:any){
		let url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/medical-history/antecendets";
		this.util.load(1);
		data.api_token= this.auth.userL.user.api_token;
		this.issuesPromise=  this.http.post(url, data).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
		
			//this.issues =data;
		}).catch(err => {
				//alert("error");
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba guardar los antecedentres" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
	}
	
	diseasesUpdate(){
		let data:any={};
		let ids= [];
		for (let i = 0; i < this.diseases.length; i++ ){
			if (this.myDiseasesMap[this.diseases[i].id] && this.myDiseasesMap[this.diseases[i].id].active){
					ids.push(this.diseases[i].id);
			}
		}
		
		let url =  AppContants.url_backend + "api/patients/"+this.auth.userL.user.id+"/diseases";
		this.util.load(1);
		data.diseases = ids;
		data.api_token= this.auth.userL.user.api_token;
		this.issuesPromise=  this.http.put(url, data).toPromise()
		this.issuesPromise.then(
		  (data:any) =>{
			  	//alert("pass");
			  this.util.unload(1);
			  this.myDiseases =data.data;
				this.myDiseasesMap=[];
				for (let i = 0; i < this.myDiseases.length; i++ ){
					this.myDiseases[i].active = true;
					this.myDiseasesMap[this.myDiseases[i].id] = this.myDiseases[i];
				}
			
		
			//this.issues =data;
		}).catch(err => {
				//alert("error");
			console.error(err);
			 this.util.unload(1);
				this.util.showToast("Ha ocurrido un error mientras se intetaba actualizar las enfermedades" ,"bottom",'Ok',2000)
		});
		return this.issuesPromise;
	}
}
