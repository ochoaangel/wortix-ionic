/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'wortix\'">' + entity + '</span>' + html;
	}
	var icons = {
		'wortixalergics1': '&#xe925;',
		'wortixneurology': '&#xe926;',
		'wortixneurology-surgery': '&#xe927;',
		'wortixgastroenterologist': '&#xe900;',
		'wortixhiv': '&#xe901;',
		'wortixinternal-medicine': '&#xe902;',
		'wortixlaboratory': '&#xe903;',
		'wortixlung': '&#xe904;',
		'wortixmaxiofacial-surgery': '&#xe905;',
		'wortixmicrobiology': '&#xe906;',
		'wortixnefrology': '&#xe907;',
		'wortixneumology': '&#xe908;',
		'wortixodontology': '&#xe909;',
		'wortixendocrinology': '&#xe90a;',
		'wortixesthetic-surgery': '&#xe90b;',
		'wortixgeneral-medicine': '&#xe90c;',
		'wortixgeneral-surgery': '&#xe90d;',
		'wortixgenetics': '&#xe90e;',
		'wortixgeriatrics': '&#xe90f;',
		'wortixginecology': '&#xe910;',
		'wortixhematology': '&#xe911;',
		'wortixinmunology': '&#xe912;',
		'wortixalergics': '&#xe913;',
		'wortixcardiology': '&#xe914;',
		'wortixchest-surgery': '&#xe915;',
		'wortixdermatology': '&#xe916;',
		'wortixear-trhoat': '&#xe917;',
		'wortixemergency': '&#xe918;',
		'wortixfetus': '&#xe919;',
		'wortixflasks': '&#xe91a;',
		'wortixpharmacy': '&#xe91b;',
		'wortixphysiatrist': '&#xe91c;',
		'wortixpregnant': '&#xe91d;',
		'wortixpreventive-medicine': '&#xe91e;',
		'wortixpsicology': '&#xe91f;',
		'wortixpsiquiatry': '&#xe920;',
		'wortixsexology': '&#xe921;',
		'wortixsyringe': '&#xe922;',
		'wortixtraumatology': '&#xe923;',
		'wortixurology': '&#xe924;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/wortix[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
