import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { AppoimentsPage } from './appoiments';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AppoimentsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppoimentsPage), TranslateModule,
		ComponentsModule,
  ],
})
export class AppoimentsPageModule {}
