import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilProvider } from '../../providers/util/util';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
	pet:string='kittens';
	newUser:any={};
	userL:any={
		email:"",
		password:""
	}

  constructor(public navCtrl: NavController, public vc: ViewController, public navParams: NavParams,  public serviceAuth: AuthProvider, public util: UtilProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  
  cancel(){
	  this.vc.dismiss();
  }
  
  nuevaCuenta(user_data:any){
	  this.serviceAuth.signUpByEmail(user_data,1).then( data=> {
		  this.vc.dismiss();
		  console.log("respuesta: ", data);
	  }, e => {
		this.util.showToast(e ,"bottom",'Ok',2000);
		  
	  }).catch( e => {
			console.log(e)
		  
		}
	  
	  )
	  
  }
  
  loginCuenta(email:string, password:string){
	  console.log(this.userL);
	  this.serviceAuth.singInByEmail(email, password).then( data=> {
		  console.log();
		  this.vc.dismiss(data);
		  console.log("respuesta: ", data);
		  
	  }, e => {
			this.util.showToast(e ,"bottom",'Ok',2000);
		  //this.util.(e);
		  
	  }).catch( e => {
			console.log(e)
		  
		}
	  
	  )
	  
  }
  
  social_login(){}
  

	
}
