import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { SpecialiteProvider } from '../../providers/specialite/specialite'

/**
 * Generated class for the AcountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-acount',
  templateUrl: 'acount.html',
})
export class AcountPage {
tipo:any;
categories:any;
userActual:any={};
tiposUpdate:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public authProvider: AuthProvider, public providerSpacialite: SpecialiteProvider ) {
  }

  ionViewWillLoad() {
	 
	this.tipo = this.navParams.get('type') || localStorage.tipo || false;
	
	if (!this.tipo){
		this.tipo =1;
	}
		 localStorage.tipo = this.tipo;
	
	if (this.tipo != 'registerDoctor'){
		this.authProvider.whoIam().then(()=>this.userActual= this.authProvider.userL.user);
		
	}else{
		this.userActual= {}
	}
	
	this.providerSpacialite.get_all().then( data => {
				this.categories = this.providerSpacialite.specialties;
				
			}
		);
  }
  
  registerDoctor(){
		let toSignUp = {
			name: this.userActual.name,
			last_name: this.userActual.last_name,
			email: this.userActual.email,
			password: this.userActual.password,
			display_name: this.userActual.display_name,
			firebaseToken: ''
			
		}
	   this.authProvider.signUpByEmail(toSignUp, 2).then( data=> {
		  Object.assign(this.authProvider.userL.user, this.userActual);
		  this.updateDoctor();
		 // console.log("respuesta: ", data);
	  }, e => {
		  alert(e);
		  
	  }).catch( e => {
			console.log(e)
		  
		}
	  )
  }
  
  
   updateDoctor(){
	  console.log(this.authProvider.userL.user);
		
	   this.authProvider.saveMe(this.tiposUpdate).then( data=> {
		  //this.vc.dismiss();
		  console.log("respuesta: ", data);
	  }, e => {
		  alert(e);
		  
	  }).catch( e => {
			console.log(e)
		  
		}
	  )
	  
  }
  
  addTipo(tipo:string){
	
	  let tipos = [];
	  let found  = false;
	  for (let i =0; i < this.tiposUpdate.length; i++){
		  if ( this.tiposUpdate[i] == tipo){
			  found = true;
		  }else{
			  tipos.push(this.tiposUpdate[i]);
		  }
	  }
	  if (!found){
		   tipos.push(tipo);
	  }
	  this.tiposUpdate = tipos;
	  console.log(this.tiposUpdate);
  }

}
