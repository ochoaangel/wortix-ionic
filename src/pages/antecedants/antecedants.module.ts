import { NgModule } from '@angular/core'; import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { AntecedantsPage } from './antecedants';

@NgModule({
  declarations: [
    AntecedantsPage,
  ],
  imports: [
    IonicPageModule.forChild(AntecedantsPage), TranslateModule,
  ],
})
export class AntecedantsPageModule {}
